FROM python:2

ARG app_name=webform
ENV app_name=${app_name}

WORKDIR /code

COPY ./ .

RUN \
    cat bashrc >>~/.bashrc \
    && useradd user \
    && apt-get update \
    && apt-get install -y \
        postgresql-client \
    && pip install -r requirements.txt

ENV QUEUE=${app_name}-default

ENTRYPOINT [ "python" ]
CMD [ "-i" ]

USER user
