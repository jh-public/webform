from rest_framework     import serializers
from feedback.models    import *


class ChoiceSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Choices
        fields = (
            "choice_category",
            "name",
            "responses_limit",
            "sort_order",
        )


class ChoiceCategorySerializer( serializers.ModelSerializer ):
    choices_set = ChoiceSerializer( many=True )
    class Meta:
        model = ChoiceCategories
        fields = (
            "name",
            'choices_set',
        )


class QuestionSerializer( serializers.ModelSerializer ):
    choice_category = ChoiceCategorySerializer()

    class Meta:
        model = Questions
        fields = (
            "id",
            "question_no",
            "question",
            "answer_type",
            "choice_category",
            "options",
            "options_columns",
            "required",
            "unique",
            "page_break",
            "depends_on",
            "depends_value",
            "validators",
            "col_widths",
            "col_offsets",
            "on_newline",
            "msg_err_unique",
            "minimum",
            "maximum",
            "email_input_again",
            "email_send_confirmation",
            "email_confirmation_text",
            "email_from",
            "email_cc",
            "email_bcc",
        )


class FormDetailSerializer( serializers.ModelSerializer ):
    questions_set = QuestionSerializer( many=True )
    class Meta:
        model = Projects
        fields = (
            "id",
            "reference",
            "description",
            "status",
            "expiry",
            "redirect_url",
            "redirect_text",
            "closure_url",
            "closure_text",
            "max_feedbacks",
            "max_feedbacks_url",
            "max_feedbacks_text",
            "notify",
            "template",
            "queue",
            "ga_code",
            "consent_page",
            'questions_set',
            # "perms_read",
            # "perms_write",
            # "created_by",
            # "created_date",
        )


class FormListSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Projects
        fields = (
            "id",
            "reference",
            "description",
        )

