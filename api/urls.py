from django.conf.urls               import url, include
from rest_framework                 import routers
from rest_framework.renderers       import CoreJSONRenderer
from api.serializers                import *

import api.views as views


router = routers.DefaultRouter(
    schema_title="Forms API",
    schema_renderers=[ CoreJSONRenderer ],
)

router.register( r'forms', views.FormViewSet )


urlpatterns = [
    url( r'^', include( router.urls )),
]
