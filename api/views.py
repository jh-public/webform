from rest_framework                                     import viewsets, permissions
from rest_framework.views                               import APIView
from rest_framework.permissions                         import BasePermission, IsAuthenticated, DjangoModelPermissions
from rest_framework.response                            import Response

from django.http                                        import Http404
from oauth2_provider.ext.rest_framework                 import TokenHasScope, TokenHasReadWriteScope, TokenHasResourceScope
from oauth2_provider.ext.rest_framework.authentication  import OAuth2Authentication
from pdb                                                import set_trace as st

from api.serializers                                    import *
from feedback.models                                    import *


class IsAuthenticatedOrTokenHasScope(BasePermission):
    """
    The user is authenticated using some backend or the token has the right scope
    This only returns True if the user is authenticated, but not using a token
    or using a token, and the token has the correct scope.
    This is usefull when combined with the DjangoModelPermissions to allow people browse the browsable api's
    if they log in using the a non token bassed middleware,
    and let them access the api's using a rest client with a token
    """
    def has_permission(self, request, view):
        is_authenticated = IsAuthenticated().has_permission(request, view)
        oauth2authenticated = False
        if is_authenticated:
            oauth2authenticated = isinstance(request.successful_authenticator, OAuth2Authentication)

        token_has_scope = TokenHasScope()
        return (is_authenticated and not oauth2authenticated) or token_has_scope.has_permission(request, view)



class FormViewSet( viewsets.ReadOnlyModelViewSet ):

    queryset            = Projects.objects.all()
    serializer_class    = FormListSerializer
    required_scopes     = [ 'forms' ]
    permission_classes  = [ IsAuthenticatedOrTokenHasScope, DjangoModelPermissions ]


    def list( self, request, format=None ):
        if not request.user.has_perm( "feedback.change_projects" ):
            raise Http404( "Invalid user" )
        objects     = Projects.objects.all()
        serializer  = FormListSerializer( objects, many=True )
        return Response( serializer.data )

    def retrieve( self, request, pk, format=None ):
        if not request.user.has_perm( "feedback.change_projects" ):
            raise Http404( "Invalid user" )
        try:
            try:
                pk = int( pk )
                object = Projects.objects.get( pk = pk )
            except ValueError:
                object = Projects.objects.filter( reference = pk ).get()
        except Projects.DoesNotExist:
            raise Http404

        data = FormDetailSerializer( object ).data
        return Response( data )

