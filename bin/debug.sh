#!/usr/bin/env bash

project=$( basename $PWD )
image=${project}_app

run(){
    docker run -it --rm \
        -v $PWD:/code \
        --link ${project}_db_1 \
        --network ${project}_default \
        -p 8000 \
        --name ${project}_debug \
        $image 
}

open(){
    container=${project}_debug
    for i in $( seq 20 ); do
        if port="$( docker port $container 2>&1)"; then
            break
        fi
        sleep 1
    done
    port=$( echo $port | cut -d: -f2 )
    xdg-open http://localhost:$port
}

main(){
    open &
    echo $$
    run
}
main "$@"
