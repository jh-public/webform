service=app
while getopts "s:u:" opt; do
    case $opt in 
        s) service=$OPTARG;;
        u) user=" --user $OPTARG ";;
    esac
done
shift $((OPTIND-1))

command="${*:-bash}"
docker-compose exec ${user} ${service} ${command}

