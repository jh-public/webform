workers=1
project=project
while getopts "p:q:c:" opt; do
    case $opt in
        p) project=$OPTARG;;
        q) queue=$OPTARG ;;
        c) workers=$OPTARG ;;
    esac
done
shift $((OPTIND-1))
: ${queue:?usage: $(basename $0) -q QUEUE [-c #workers]}

echo "Creating $workers worker(s) for queue $queue..."
docker-compose exec -d queue \
    celery \
        -A $project worker \
        -Q $queue \
        -n $queue \
        -c $workers
docker-compose exec queue celery -A $project status

