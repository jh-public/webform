#!/usr/bin/env bash

wait_for_db(){
    while ! pg_isready -h db; do
        sleep 1
    done
}

create_default_user(){
./manage.py shell<<EOD
from django.contrib.auth.models import User
try:
    User.objects.get( username='admin' )
except User.DoesNotExist as e:
    User.objects.create_superuser( 'admin', '', 'xxx12345' )
EOD
}

main(){
    cd /code
    wait_for_db
    ./manage.py migrate
    ./manage.py collectstatic --noinput
    create_default_user
    [ -e ./loaddata.sh ] && ./loaddata.sh
    ./manage.py runserver 0.0.0.0:8000
}
main "$@"
