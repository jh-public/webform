from __future__ import print_function
from feedback.models import *
from pprint import pprint as pp
from pdb    import set_trace as st
import sys

def x():
    import feedback
    qs = feedback.models.Answers.objects.filter( question__pk__gte = 100 )
    print( qs.count() )

qs = Answers.objects.all()
qs = qs.filter( question__pk__lte = 100 )
for i, aa in enumerate( qs ):
    print( i, end="\r" )
    if i % 100 == 0:
        sys.stdout.flush()
    try:
        qq = Questions.objects.get( project = aa.project.id, question_no = aa.question.pk )
    except Exception as e:
        print ( "\n*****\n", aa.question.pk, e )
        continue
    aa.question = qq
    aa.save()
