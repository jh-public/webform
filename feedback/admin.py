from django.contrib     import admin
from feedback.models    import *



# Projects
class InlineQuestions(admin.TabularInline):
    model           = Questions
    extra           = 0

class ProjectAdmin(admin.ModelAdmin):
    form            = QuestionForm
    list_display    = [ "reference", "description", "expiry", "status", "template", "max_feedbacks", "queue", "created_by" ]
    inlines         = [ InlineQuestions, ]
    ordering        = [ "reference", ]
    list_filter     = ( "status", "template", "queue", "created_by", )
    search_fields   = ( "reference", "description", )



# Questions
class QuestionAdmin(admin.ModelAdmin):
    list_display = [ "project", "question_no", "question", "answer_type", "depends_on", "depends_value" ]
    list_filter  = ( "project__reference", )
    ordering     = [ "project", "question_no", ]



# Feedbacks
class InlineAnswers(admin.TabularInline):
    model           = Answers
    extra           = 0

class FeedbackAdmin(admin.ModelAdmin):
    list_filter     = ( "project__reference",)
    list_display    = [ "project", "timestamp", "user_agent", "payment_received" ]
    inlines         = [ InlineAnswers, ]
    ordering        = [ "project", "-timestamp" ]



# Answers
class AnswerAdmin(admin.ModelAdmin):
    list_display    = [ "project", "feedback", "question", "answer" ]
    list_filter     = ( "project__reference", "question__question", "feedback" )
    search_fields   = ( "question__question", )
    ordering        = [ "project", "feedback", "question" ]



# AnswerEdits
class AnswerEditAdmin(admin.ModelAdmin):
    list_display_links = None
    def has_delete_permission(self, request, obj=None):
        return False

# Choices
class ChoicesAdmin(admin.ModelAdmin):
    list_display    = ( "choice_category", "name", "responses_limit" )
    search_fields   = ( "name", )
    ordering        = ( "choice_category__name", "sort_order", "name" )



# ChoiceCategories
class InlineChoices(admin.TabularInline):
    model           = Choices
    extra           = 1

class ChoiceCategoryAdmin(admin.ModelAdmin):
    inlines         = [ InlineChoices, ]



# ApplicationErrorLog
class ApplicationErrorLogAdmin(admin.ModelAdmin):
    list_display    = ( "project", "question_no", "error", "timestamp" )
    list_filter     = ( "project__reference", "question_no", "error" )
    ordering        = ( "-timestamp", )



admin.site.register(Projects,               ProjectAdmin)
admin.site.register(Questions,              QuestionAdmin)
admin.site.register(Feedbacks,              FeedbackAdmin)
admin.site.register(ChoiceCategories,       ChoiceCategoryAdmin)
admin.site.register(Choices,                ChoicesAdmin)
admin.site.register(ApplicationErrorLog,    ApplicationErrorLogAdmin)
admin.site.register(AnswerEdits,            AnswerEditAdmin)
