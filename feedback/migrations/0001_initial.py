# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Answers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('answer', models.TextField()),
            ],
            options={
                'ordering': ['project', 'feedback', 'question__question_no'],
                'verbose_name_plural': 'Answers',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Feedbacks',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['project', '-timestamp'],
                'verbose_name_plural': 'Feedbacks',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Projects',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reference', models.CharField(max_length=16)),
                ('description', models.TextField()),
            ],
            options={
                'ordering': ['reference'],
                'verbose_name_plural': 'Projects',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Questions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question_no', models.IntegerField(default=1)),
                ('question', models.TextField()),
                ('answer_type', models.CharField(default=b'radio', max_length=16, choices=[(b'radio', b'Multiple choice'), (b'checkbox', b'Yes/No'), (b'text', b'Single line text'), (b'textarea', b'Multi-line text'), (b'hidden', b'Hidden')])),
                ('options', models.TextField(null=True, blank=True)),
                ('required', models.BooleanField(default=False)),
                ('project', models.ForeignKey(to='feedback.Projects')),
            ],
            options={
                'ordering': ['project', 'question_no'],
                'verbose_name_plural': 'Questions',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='feedbacks',
            name='project',
            field=models.ForeignKey(to='feedback.Projects'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answers',
            name='feedback',
            field=models.ForeignKey(to='feedback.Feedbacks'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answers',
            name='project',
            field=models.ForeignKey(to='feedback.Projects'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='answers',
            name='question',
            field=models.ForeignKey(to='feedback.Questions'),
            preserve_default=True,
        ),
    ]
