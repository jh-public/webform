# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0002_questions_page_break'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='depends_on',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='questions',
            name='depends_value',
            field=models.CharField(default='', max_length=255, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='questions',
            name='options',
            field=models.TextField(blank=True),
        ),
    ]
