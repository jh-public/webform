# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0004_auto_20141016_1854'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='unique',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='questions',
            name='validator',
            field=models.CharField(default='', max_length=16, blank=True, choices=[(b'email', b'email')]),
            preserve_default=False,
        ),
    ]
