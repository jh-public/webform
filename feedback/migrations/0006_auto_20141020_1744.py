# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0005_auto_20141018_2211'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='questions',
            name='unique',
        ),
        migrations.RemoveField(
            model_name='questions',
            name='validator',
        ),
        migrations.AddField(
            model_name='questions',
            name='validators',
            field=models.TextField(default='', blank=True, choices=[(b'unique', b'Unique'), (b'email', b'Email')]),
            preserve_default=False,
        ),
    ]
