# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0006_auto_20141020_1744'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='validators',
            field=models.CharField(blank=True, max_length=255, choices=[(b'unique', b'Unique'), (b'email', b'Email')]),
        ),
    ]
