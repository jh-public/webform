# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0007_auto_20141020_1942'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='validators',
            field=models.CommaSeparatedIntegerField(blank=True, max_length=255, null=True, choices=[(0, b'Unique'), (1, b'Email')]),
        ),
    ]
