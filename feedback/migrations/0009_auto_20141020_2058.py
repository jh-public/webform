# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0008_auto_20141020_2010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='validators',
            field=models.CommaSeparatedIntegerField(default=b'0', max_length=255, blank=True),
        ),
    ]
