# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0009_auto_20141020_2058'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='answer_type',
            field=models.CharField(default=b'radio', max_length=16, choices=[(b'radio', b'Multiple choice'), (b'multicheckbox', b'Multiple answer'), (b'checkbox', b'Yes/No'), (b'text', b'Single line text'), (b'textarea', b'Multi-line text'), (b'readonly', b'Read-only'), (b'hidden', b'Hidden')]),
        ),
    ]
