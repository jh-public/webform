# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0010_auto_20141028_1211'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='perms_read',
            field=models.CommaSeparatedIntegerField(default=b'', max_length=255, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='questions',
            name='perms_write',
            field=models.CommaSeparatedIntegerField(default=b'', max_length=255, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='questions',
            name='status',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
