# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0011_auto_20141029_1452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='status',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
