# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0012_auto_20141029_1534'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='questions',
            name='perms_read',
        ),
        migrations.RemoveField(
            model_name='questions',
            name='perms_write',
        ),
        migrations.RemoveField(
            model_name='questions',
            name='status',
        ),
        migrations.AddField(
            model_name='projects',
            name='perms_read',
            field=models.CommaSeparatedIntegerField(default=b'', max_length=255, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='projects',
            name='perms_write',
            field=models.CommaSeparatedIntegerField(default=b'', max_length=255, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='projects',
            name='status',
            field=models.IntegerField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
