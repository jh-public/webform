# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0014_feedbacks_username'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answers',
            name='feedback',
            field=models.ForeignKey(to='feedback.Feedbacks', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='answers',
            name='project',
            field=models.ForeignKey(to='feedback.Projects', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='answers',
            name='question',
            field=models.ForeignKey(to='feedback.Questions', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='feedbacks',
            name='project',
            field=models.ForeignKey(to='feedback.Projects', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='questions',
            name='project',
            field=models.ForeignKey(to='feedback.Projects', on_delete=django.db.models.deletion.PROTECT),
            preserve_default=True,
        ),
    ]
