# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0015_auto_20141030_0917'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='redirect_text',
            field=models.TextField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='questions',
            name='redirect_url',
            field=models.URLField(default='', blank=True),
            preserve_default=False,
        ),
    ]
