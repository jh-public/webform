# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0016_auto_20141101_1835'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='questions',
            name='redirect_text',
        ),
        migrations.RemoveField(
            model_name='questions',
            name='redirect_url',
        ),
        migrations.AddField(
            model_name='projects',
            name='redirect_text',
            field=models.TextField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projects',
            name='redirect_url',
            field=models.URLField(default='', blank=True),
            preserve_default=False,
        ),
    ]
