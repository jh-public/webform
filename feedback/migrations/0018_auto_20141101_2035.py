# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0017_auto_20141101_1927'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='closure_text',
            field=models.TextField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projects',
            name='closure_url',
            field=models.URLField(default='', blank=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projects',
            name='expiry',
            field=models.DateField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
