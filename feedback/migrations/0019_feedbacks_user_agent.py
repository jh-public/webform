# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0018_auto_20141101_2035'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbacks',
            name='user_agent',
            field=models.CharField(default=b'', max_length=255, blank=True),
            preserve_default=True,
        ),
    ]
