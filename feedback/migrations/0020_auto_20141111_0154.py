# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0019_feedbacks_user_agent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='depends_on',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='feedback.Questions', null=True),
            preserve_default=True,
        ),
    ]
