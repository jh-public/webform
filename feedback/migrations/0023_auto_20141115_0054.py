# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0022_auto_20141114_1743'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projects',
            name='reference',
            field=models.CharField(default=b'', unique=True, max_length=255, error_messages={b'unique': b'Duplicate reference'}),
            preserve_default=True,
        ),
    ]
