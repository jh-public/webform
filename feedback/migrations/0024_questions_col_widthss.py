# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0023_auto_20141115_0054'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='col_widthss',
            field=models.CommaSeparatedIntegerField(default=b'12,12,12,12', max_length=255, blank=True),
            preserve_default=True,
        ),
    ]
