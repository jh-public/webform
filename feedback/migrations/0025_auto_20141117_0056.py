# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0024_questions_col_widthss'),
    ]

    operations = [
        migrations.RenameField(
            model_name='questions',
            old_name='col_widthss',
            new_name='col_widths',
        ),
    ]
