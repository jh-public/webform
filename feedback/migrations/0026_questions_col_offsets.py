# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0025_auto_20141117_0056'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='col_offsets',
            field=models.CommaSeparatedIntegerField(default=b'0,0,0,0', max_length=255, blank=True),
            preserve_default=True,
        ),
    ]
