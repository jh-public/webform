# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0026_questions_col_offsets'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='on_newline',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
