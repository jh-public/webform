# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0027_questions_on_newline'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='options_columns',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
