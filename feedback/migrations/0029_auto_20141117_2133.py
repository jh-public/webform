# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0028_questions_options_columns'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='options_columns',
            field=models.IntegerField(default=12, choices=[(12, b'1 column'), (6, b'2 columns'), (4, b'3 columns'), (3, b'4 columns')]),
            preserve_default=True,
        ),
    ]
