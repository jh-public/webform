# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0030_auto_20141118_0246'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='options',
            field=models.TextField(default=b'', verbose_name=b'Answer Choices', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='questions',
            name='options_columns',
            field=models.IntegerField(default=12, verbose_name=b'Choices Layout', choices=[(12, b'1 column'), (6, b'2 columns'), (4, b'3 columns'), (3, b'4 columns')]),
            preserve_default=True,
        ),
    ]
