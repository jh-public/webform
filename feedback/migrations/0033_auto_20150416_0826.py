# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0032_auto_20150314_1902'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projects',
            name='status',
            field=models.IntegerField(default=1, null=True, blank=True),
            preserve_default=True,
        ),
    ]
