# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0033_auto_20150416_0826'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='msg_err_duplicate',
            field=models.TextField(default=b'', blank=True),
            preserve_default=True,
        ),
    ]
