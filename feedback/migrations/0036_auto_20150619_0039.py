# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0035_auto_20150613_1335'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projects',
            name='msg_err_duplicate',
        ),
        migrations.AddField(
            model_name='questions',
            name='msg_err_unique',
            field=models.TextField(default=b'', blank=True),
            preserve_default=True,
        ),
    ]
