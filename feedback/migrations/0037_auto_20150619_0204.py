# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0036_auto_20150619_0039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='msg_err_unique',
            field=models.TextField(default=b'', verbose_name=b'Unique validator error message', blank=True),
            preserve_default=True,
        ),
    ]
