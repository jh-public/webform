# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0038_auto_20150626_1225'),
    ]

    operations = [
        migrations.RenameField(
            model_name='projects',
            old_name='user',
            new_name='created_by',
        ),
    ]
