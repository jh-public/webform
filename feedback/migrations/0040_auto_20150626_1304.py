# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0039_auto_20150626_1233'),
    ]

    operations = [
        migrations.RenameField(
            model_name='projects',
            old_name='date_created',
            new_name='created_date',
        ),
        migrations.AlterField(
            model_name='projects',
            name='created_by',
            field=models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
