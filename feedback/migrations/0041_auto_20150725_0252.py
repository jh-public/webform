# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0040_auto_20150626_1304'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='feedbacks',
            options={'ordering': ['project', '-timestamp'], 'verbose_name_plural': 'Forms'},
        ),
    ]
