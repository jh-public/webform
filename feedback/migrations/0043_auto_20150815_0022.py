# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0042_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='max_feedbacks',
            field=models.IntegerField(default=0, blank=True),
        ),
        migrations.AddField(
            model_name='projects',
            name='max_feedbacks_text',
            field=models.TextField(default=b'', blank=True),
        ),
    ]
