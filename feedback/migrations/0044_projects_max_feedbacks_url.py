# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0043_auto_20150815_0022'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='max_feedbacks_url',
            field=models.URLField(default=b'', blank=True),
        ),
    ]
