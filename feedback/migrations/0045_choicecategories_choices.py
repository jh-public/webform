# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0044_projects_max_feedbacks_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChoiceCategories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=16)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name_plural': 'Choice Categories',
            },
        ),
        migrations.CreateModel(
            name='Choices',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32)),
                ('responses_limit', models.IntegerField(default=0, blank=True)),
                ('choice_category', models.ForeignKey(to='feedback.ChoiceCategories')),
            ],
            options={
                'ordering': ['choice_category', 'name'],
                'verbose_name_plural': 'Choices',
            },
        ),
    ]
