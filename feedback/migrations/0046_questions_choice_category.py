# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0045_choicecategories_choices'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='choice_category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, verbose_name=b'Choices Type', blank=True, to='feedback.ChoiceCategories', null=True),
        ),
    ]
