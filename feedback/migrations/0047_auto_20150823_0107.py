# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0046_questions_choice_category'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='choices',
            options={'ordering': ['choice_category', 'sort_order', 'name'], 'verbose_name_plural': 'Choices'},
        ),
        migrations.AddField(
            model_name='choices',
            name='sort_order',
            field=models.IntegerField(default=99, blank=True),
        ),
    ]
