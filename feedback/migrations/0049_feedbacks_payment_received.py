# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0048_auto_20150904_1643'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedbacks',
            name='payment_received',
            field=models.BooleanField(default=False),
        ),
    ]
