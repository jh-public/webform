# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0049_feedbacks_payment_received'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choices',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
