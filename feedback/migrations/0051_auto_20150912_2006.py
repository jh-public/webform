# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0050_auto_20150912_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='answer_type',
            field=models.CharField(default=b'radio', max_length=16, choices=[(b'radio', b'Multiple choices - single answer'), (b'multicheckbox', b'Multiple choices - multiple answers'), (b'checkbox', b'Yes/No'), (b'text', b'Single line text'), (b'textarea', b'Multi-line text'), (b'number', b'Number'), (b'date', b'Date'), (b'month', b'Month'), (b'label', b'Label'), (b'readonly', b'Read-only'), (b'hidden', b'Hidden'), (b'paypal', b'Paypal')]),
        ),
    ]
