# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0051_auto_20150912_2006'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='template',
            field=models.CharField(default=b'default.html', max_length=255, blank=True),
        ),
    ]
