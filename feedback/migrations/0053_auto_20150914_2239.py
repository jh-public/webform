# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0052_projects_template'),
    ]

    operations = [
        migrations.AlterField(
            model_name='choicecategories',
            name='name',
            field=models.CharField(max_length=100),
        ),
    ]
