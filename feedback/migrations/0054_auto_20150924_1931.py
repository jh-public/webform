# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0053_auto_20150914_2239'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='email_confirmation_text',
            field=models.TextField(default=b'', blank=True),
        ),
        migrations.AddField(
            model_name='questions',
            name='email_input_again',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='questions',
            name='email_send_confirmation',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='questions',
            name='answer_type',
            field=models.CharField(default=b'radio', max_length=16, choices=[(b'radio', b'Multiple choices - single answer'), (b'multicheckbox', b'Multiple choices - multiple answers'), (b'checkbox', b'Yes/No'), (b'text', b'Single line text'), (b'textarea', b'Multi-line text'), (b'number', b'Number'), (b'email', b'Email'), (b'date', b'Date'), (b'month', b'Month'), (b'label', b'Label'), (b'readonly', b'Read-only'), (b'hidden', b'Hidden'), (b'paypal', b'Paypal')]),
        ),
    ]
