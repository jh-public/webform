# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0054_auto_20150924_1931'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='feedbacks',
            options={'ordering': ['project', '-timestamp'], 'verbose_name_plural': 'Feedbacks'},
        ),
        migrations.AlterModelOptions(
            name='projects',
            options={'ordering': ['reference'], 'verbose_name_plural': 'Forms'},
        ),
    ]
