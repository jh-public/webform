# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('feedback', '0055_auto_20150927_2136'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnswerEdits',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('reason', models.TextField()),
                ('old_answer', models.TextField(blank=True)),
                ('new_answer', models.TextField()),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('answer', models.ForeignKey(to='feedback.Answers', on_delete=django.db.models.deletion.PROTECT)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.PROTECT)),
            ],
            options={
                'ordering': ['-timestamp'],
                'verbose_name_plural': 'Answers',
            },
        ),
        migrations.AlterField(
            model_name='feedbacks',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
