# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0056_auto_20150928_0036'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='answeredits',
            options={'ordering': ['-timestamp'], 'verbose_name_plural': 'Answer Edits'},
        ),
    ]
