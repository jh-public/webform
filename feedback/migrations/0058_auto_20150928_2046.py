# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0057_auto_20150928_1837'),
    ]

    operations = [
        migrations.AddField(
            model_name='questions',
            name='email_bcc',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='questions',
            name='email_cc',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='questions',
            name='email_from',
            field=models.CharField(default=b'noreply@newcreation.org.sg', max_length=255, blank=True),
        ),
    ]
