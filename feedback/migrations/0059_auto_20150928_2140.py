# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0058_auto_20150928_2046'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='email_from',
            field=models.CharField(default=b'no-reply@newcreation.org.sg', max_length=255, blank=True),
        ),
    ]
