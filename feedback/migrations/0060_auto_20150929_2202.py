# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0059_auto_20150928_2140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='email_bcc',
            field=models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(b'.+\\@newcreation.org.sg', b'NCC addresses only')]),
        ),
        migrations.AlterField(
            model_name='questions',
            name='email_cc',
            field=models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(b'.+\\@newcreation.org.sg', b'NCC addresses only')]),
        ),
        migrations.AlterField(
            model_name='questions',
            name='email_from',
            field=models.CharField(default=b'no-reply@newcreation.org.sg', max_length=255, blank=True, validators=[django.core.validators.RegexValidator(b'.+\\@newcreation.org.sg', b'NCC addresses only')]),
        ),
    ]
