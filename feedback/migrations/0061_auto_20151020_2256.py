# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0060_auto_20150929_2202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answers',
            name='answer',
            field=models.TextField(db_index=True),
        ),
        migrations.AlterField(
            model_name='choicecategories',
            name='name',
            field=models.CharField(max_length=100, db_index=True),
        ),
        migrations.AlterField(
            model_name='choices',
            name='name',
            field=models.CharField(max_length=100, db_index=True),
        ),
        migrations.AlterField(
            model_name='projects',
            name='reference',
            field=models.CharField(default=b'', unique=True, max_length=255, db_index=True, error_messages={b'unique': b'Duplicate reference'}),
        ),
        migrations.AlterField(
            model_name='projects',
            name='status',
            field=models.IntegerField(default=1, null=True, db_index=True, blank=True),
        ),
        migrations.AlterField(
            model_name='questions',
            name='question_no',
            field=models.IntegerField(default=1, db_index=True),
        ),
    ]
