# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0061_auto_20151020_2256'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='queue',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
    ]
