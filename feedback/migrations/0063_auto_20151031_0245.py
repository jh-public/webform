# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0062_projects_queue'),
    ]

    operations = [
        migrations.AddField(
            model_name='projects',
            name='ga_code',
            field=models.CharField(default=b'', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='projects',
            name='status',
            field=models.IntegerField(default=1, null=True, db_index=True, blank=True, choices=[(-1, b'Deleted'), (0, b'Authenticated use'), (0, b'Anonymous use')]),
        ),
    ]
