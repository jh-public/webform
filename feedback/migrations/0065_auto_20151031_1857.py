# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0064_auto_20151031_1530'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projects',
            name='closure_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex=b'https?://(\\w.*?\\.)?(newcreation.org.sg)|(noah.sg)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='projects',
            name='max_feedbacks_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex=b'https?://(\\w.*?\\.)?(newcreation.org.sg)|(noah.sg)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='projects',
            name='redirect_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex=b'https?://(\\w.*?\\.)?(newcreation.org.sg)|(noah.sg)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='projects',
            name='status',
            field=models.IntegerField(default=1, null=True, db_index=True, blank=True, choices=[(-1, b'Deleted'), (0, b'Authenticated use'), (1, b'Anonymous use')]),
        ),
    ]
