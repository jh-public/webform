# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0066_auto_20151101_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questions',
            name='validators',
            field=models.CommaSeparatedIntegerField(default=b'0', max_length=255, null=True, blank=True),
        ),
    ]
