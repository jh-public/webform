# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


def convert_validators( apps, schema_editor ):

    Questions = apps.get_model("feedback", "Questions")
    for question in Questions.objects.all():

        if not question.validators:
            continue

        validators = question.validators.split(",")

        if "1" in validators:
            question.unique = True

        if "2" in validators:
            question.answer_type = "email"

        if "3" in validators:
            question.answer_type = "nric"

        question.save()


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0068_auto_20151101_2251'),
    ]

    operations = [
        migrations.RunPython( convert_validators, migrations.RunPython.noop ),

    ]
