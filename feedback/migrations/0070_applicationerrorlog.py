# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0069_convert_validators'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationErrorLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('error', models.CharField(max_length=255)),
                ('question_no', models.IntegerField(null=True)),
                ('answer', models.TextField(null=True)),
                ('request', models.TextField(null=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('project', models.ForeignKey(to='feedback.Projects')),
            ],
        ),
    ]
