# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('feedback', '0073_auto_20151120_2027'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feedbacks',
            name='timestamp',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
        migrations.AlterField(
            model_name='projects',
            name='closure_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex='https?://(\\w.*?\\.)?(example.com)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='projects',
            name='max_feedbacks_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex='https?://(\\w.*?\\.)?(example.com)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='projects',
            name='redirect_url',
            field=models.URLField(default=b'', blank=True, validators=[django.core.validators.URLValidator(regex='https?://(\\w.*?\\.)?(example.com)/.+', message=b'NCC URLs only')]),
        ),
        migrations.AlterField(
            model_name='questions',
            name='email_bcc',
            field=models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(regex='.+\\@(example.com)', message=b'NCC addresses only')]),
        ),
        migrations.AlterField(
            model_name='questions',
            name='email_cc',
            field=models.CharField(blank=True, max_length=255, null=True, validators=[django.core.validators.RegexValidator(regex='.+\\@(example.com)', message=b'NCC addresses only')]),
        ),
        migrations.AlterField(
            model_name='questions',
            name='email_from',
            field=models.CharField(default=b'no-reply@newcreation.org.sg', max_length=255, blank=True, validators=[django.core.validators.RegexValidator(regex='.+\\@(example.com)', message=b'NCC addresses only')]),
        ),
    ]
