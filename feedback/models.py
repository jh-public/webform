from    django.contrib.auth.models      import Group, User
from    django.contrib.sites.models     import Site
from    django.core.validators          import RegexValidator, URLValidator, validate_comma_separated_integer_list
from    django.db                       import models
from    django.db.models                import Q
from    django.db.utils                 import ProgrammingError
from    django.forms.models             import model_to_dict
from    django                          import forms

import  datetime
import  itertools
import  json
import  logging
import  os
import  re
import  sys

import  project.settings as settings

from    pdb import set_trace as st

#############################################

def get_user_group_names(user):
    return ( g.name for g in user.groups.order_by('name') )


def get_user_group_ids(user):
    return (g.pk for g in user.groups.all())


def get_user_ministries(user):
    return list({ name.split("-")[0] for name in get_user_group_names(user) if len(name.split("-")) > 1 })


def get_ministry_groups(ministry_list):
    qs  = ( Group.objects.filter( name__startswith=ministry ).order_by('name') for ministry in ministry_list )
    key = lambda obj: obj.name.lower()
    return sorted(
        itertools.chain.from_iterable(qs),
        key = key,
    )

def get_user_ministry_groups(user):
    return get_ministry_groups( get_user_ministries(user) )


def access_granted(user, group_ids):
    if user.is_superuser:  return True
    group_ids       = set( [ int(gid) for gid in group_ids.split(",") ] if group_ids else [] )
    user_group_ids  = set( get_user_group_ids(user) )
    return len( group_ids & user_group_ids )

#############################################

try:
    _sites = "(%s)" % ")|(".join( site.domain.strip() for site in Site.objects.all() )
except ProgrammingError:
    _sites = "(\.*)"

ncc_email_validator = RegexValidator( regex=".+\@%s" % _sites, message="NCC addresses only" )

ncc_url_validator   = URLValidator( regex="https?://(\w.*?\.)?%s/.+" % _sites, message="NCC URLs only" )

#############################################
class MyBaseModel(models.Model):
#############################################

    class Meta:
        abstract = True

    @classmethod
    def get( cls, key ):
        q = cls.objects.filter( **{cls.key_property: key} )
        try:    return q.get()
        except  cls.DoesNotExist:
                return None

    @classmethod
    def new( cls, key ):
        entity = cls()
        setattr( entity, cls.key_property, key )
        return entity

    def model_to_dict(self):
        return model_to_dict(self)



#############################################
class ChoiceCategories(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Choice Categories"
        ordering            = [ "name", ]

    name = models.CharField( max_length=100, db_index=True )

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(ChoiceCategories, self).save(*args, **kwargs)



#############################################
class Choices(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Choices"
        ordering            = [ "choice_category", "sort_order", "name" ]

    choice_category = models.ForeignKey('ChoiceCategories', related_name="choices_set" )
    name            = models.CharField( max_length=100, db_index=True )
    responses_limit = models.IntegerField( default=0, blank=True )
    sort_order      = models.IntegerField( blank=True, default=99 )

    def __unicode__(self):
        return "%s : %s" % ( self.name, self.choice_category.name )

    def save(self, *args, **kwargs):
        self.name = self.name.strip()
        super(Choices, self).save(*args, **kwargs)



#############################################
class Projects(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Forms"
        ordering            = ["reference"]

    PROJECT_STATUSES    = (
        ( -1, "Deleted" ),
        (  0, "Authenticated use" ),
        (  1, "Anonymous use" ),
    )

    key_property        = "reference"
    reference           = models.CharField( max_length=255, default="", unique=True, error_messages={"unique":"Duplicate reference"}, db_index=True  )
    description         = models.TextField( default="")
    status              = models.IntegerField( blank=True, null=True, default=1, choices=PROJECT_STATUSES, db_index=True )
    perms_read          = models.CharField(  max_length=255, blank=True, default="", validators=[validate_comma_separated_integer_list] )
    perms_write         = models.CharField(  max_length=255, blank=True, default="", validators=[validate_comma_separated_integer_list] )
    redirect_url        = models.URLField (    blank=True,    default="", validators=[ncc_url_validator] )
    redirect_text       = models.TextField(    blank=True,    default="" )
    expiry              = models.DateField(    blank=True,    null=True  )
    closure_url         = models.URLField (    blank=True,    default="", validators=[ncc_url_validator] )
    closure_text        = models.TextField(    blank=True,    default="" )
    max_feedbacks       = models.IntegerField( blank=True,    default=0  )
    max_feedbacks_url   = models.URLField (    blank=True,    default="", validators=[ncc_url_validator] )
    max_feedbacks_text  = models.TextField(    blank=True,    default="" )
    notify              = models.TextField(    blank=True,    default="" )
    template            = models.CharField(    blank=True,    max_length=255, default="default.html" )
    queue               = models.CharField(    blank=True,    max_length=255, default="" )
    ga_code             = models.CharField(    blank=True,    max_length=255, default="" )
    consent_page        = models.BooleanField( blank=True,    default=True )
    color_font          = models.CharField(    blank=True,    max_length=32, default="" )
    color_question      = models.CharField(    blank=True,    max_length=32, default="" )
    color_accent        = models.CharField(    blank=True,    max_length=32, default="" )
    created_by          = models.ForeignKey( User, blank=True, null=True )
    created_date        = models.DateTimeField( auto_now_add=True, blank=True, null=True )


    def __unicode__(self):
        return self.reference

    def can_read(self, group):
        perms = [int(perm) for perm in self.perms_read.split(",")] if self.perms_read else []
        return group.pk in perms

    def can_write(self, group):
        perms = [int(perm) for perm in self.perms_write.split(",")] if self.perms_write else []
        return group.pk in perms


#----------------------------
class ProjectForm(forms.ModelForm):
#----------------------------
    class Meta:
        model   = Projects
        exclude = []

    perms_read  = forms.MultipleChoiceField( widget=forms.SelectMultiple )
    perms_write = forms.MultipleChoiceField( widget=forms.SelectMultiple )


    def __init__(self, *args,  **kwargs):
        user = kwargs.pop("user")
        super(ProjectForm, self).__init__(*args, **kwargs)

        # groups
        groups = [ (group.pk, group.name) for group in get_user_ministry_groups(user) ]
        self.fields["perms_read"].choices  = groups
        self.fields["perms_write"].choices = groups

        # templates
        dirname     = os.path.dirname( sys.modules[__name__].__file__ )
        dirname     = os.path.join( dirname, "templates", "questionnaire" )
        templates   = [ ( t, t.split(".")[0] ) for t in os.listdir( dirname ) if t.endswith(".html") ]
        self.fields["template"].choices = templates


    def clean_perms_read(self):
        data = self.cleaned_data['perms_read']
        return ",".join( [str(v) for v in data] )


    def clean_perms_write(self):
        data = self.cleaned_data['perms_write']
        return ",".join( [str(v) for v in data] )


    def clean_notify( self ):
        data = self.cleaned_data['notify']
        if data:
            data = [ email.strip() for email in data.split("\n") ]
            [ ncc_email_validator( email ) for email in data ]
            return "\n".join( data )
        return data

#############################################
class Questions(MyBaseModel):
#############################################

    # names must be consistent with validators.js
    VALIDATORS = (
        ( "0",          "None"),
        ( "1",          "Unique"),
        ( "2",          "Email"),
        ( "3",          "NRIC/FIN"),
    )

    ANSWER_TYPES = [
        ("radio",           "Multiple choices - single answer"),
        ("multicheckbox",   "Multiple choices - multiple answers"),
        ("checkbox",        "Yes/No"),
        ("text",            "Single line text"),
        ("textarea",        "Multi-line text"),
        ("number",          "Number"),
        ("email",           "Email"),
        ("date",            "Date"),
        ("month",           "Month"),
        ("id",              "NRIC/FIN/Passport"),
        ("nric",            "NRIC/FIN"),
        ("label",           "Label"),
        ("readonly",        "Read-only"),
        ("hidden",          "Hidden"),
    ]
    if settings.PAYPAL_REDIRECT_HOSTNAME:
        ANSWER_TYPES.append( ("paypal", "Paypal") )

    CHOICE_TYPES = [
        "radio",
        "checkbox",
        "multicheckbox"
    ]

    OPTIONS_COLUMNS = [
        # Bootstrap grid
        ( 12, "1 column"     ),
        (  6, "2 columns"    ),
        (  4, "3 columns"    ),
        (  3, "4 columns"    ),
        ( -1, "Dropdown box" ),
    ]

    STRIPTAGS = re.compile("\<.+?\>")


    class Meta:
        verbose_name_plural = "Questions"
        ordering            = ["project", "question_no"]

    project                 = models.ForeignKey(    'Projects', on_delete=models.PROTECT, related_name="questions_set" )
    question_no             = models.IntegerField(  default=1, db_index=True  )
    question                = models.TextField(     default="" )
    answer_type             = models.CharField(     max_length=16, choices=ANSWER_TYPES, default="radio" )
    choice_category         = models.ForeignKey(    'ChoiceCategories', on_delete=models.PROTECT, null=True, blank=True, verbose_name="Choices Type" )
    options                 = models.TextField(     blank=True, default="", verbose_name="Answer Choices" )
    options_columns         = models.IntegerField(  choices=OPTIONS_COLUMNS, default=12, verbose_name="Choices Layout" )
    required                = models.BooleanField(  default=False )
    unique                  = models.BooleanField(  default=False )
    page_break              = models.BooleanField(  default=False )
    depends_on              = models.ForeignKey(    'self', blank=True, null=True, on_delete=models.SET_NULL )
    depends_value           = models.CharField(     max_length=255, blank=True, default="" )
    validators              = models.CharField( max_length=255, blank=True, validators=[validate_comma_separated_integer_list], default="0", null=True )
    col_widths              = models.CharField( max_length=255, blank=True, validators=[validate_comma_separated_integer_list], default="12,12,12,12" )
    col_offsets             = models.CharField( max_length=255, blank=True, validators=[validate_comma_separated_integer_list], default="0,0,0,0" )
    on_newline              = models.BooleanField( blank=True, default=False )
    msg_err_unique          = models.TextField(    blank=True, default="", verbose_name="Unique validator error message" )
    minimum                 = models.IntegerField( blank=True, null=True )
    maximum                 = models.IntegerField( blank=True, null=True )
    email_input_again       = models.BooleanField( blank=True, default=False )
    email_send_confirmation = models.BooleanField( blank=True, default=False )
    email_confirmation_text = models.TextField(    blank=True, default="" )
    email_from              = models.CharField(    blank=True, max_length=255, default="no-reply@newcreation.org.sg", validators=[ncc_email_validator] )
    email_cc                = models.CharField(    blank=True, max_length=255, null=True, validators=[ncc_email_validator] )
    email_bcc               = models.CharField(    blank=True, max_length=255, null=True, validators=[ncc_email_validator] )


    def __unicode__(self):
        stripped = self.STRIPTAGS.sub( "", self.question )
        return "%s-%s: %s" % (self.project, self.question_no, stripped if len(stripped) < 100 else "%s..."%stripped[:99] )

    def options_split(self):
        return [ o.replace("\r","") for o in self.options.split("\n") if o.replace("\r","") ]

    def answer_blanks(self, date_from=None, date_to=None):
        qs = self.answers_set.filter(answer="")
        if date_from and date_to:
            qs = qs.filter( feedback__timestamp__range=(date_from, date_to) )
        return qs

    def answer_nonblanks(self, date_from=None, date_to=None):
        qs = self.answers_set.filter( ~Q(answer="") )
        if date_from and date_to:
            qs = qs.filter( feedback__timestamp__range=(date_from, date_to) )
        return qs

    def answer_value(self, answer, date_from=None, date_to=None):
        qs = self.answers_set.filter( answer__iexact=answer )
        if date_from and date_to:
            qs = qs.filter( feedback__timestamp__range=(date_from, date_to) )

        if not qs.count():
            qs = self.answers_set.filter( answer__iexact=answer.strip() )
            if date_from and date_to:
                qs = qs.filter( feedback__timestamp__range=(date_from, date_to) )

        return qs

    def natural_key(self):
        return self.question_no

    def is_choice_type(self):
        return self.answer_type in self.CHOICE_TYPES


#----------------------------
class QuestionForm(forms.ModelForm):
#----------------------------
    class Meta:
        model   = Questions
        exclude = []

    #validators = forms.MultipleChoiceField( widget=forms.SelectMultiple, choices=Questions.VALIDATORS, initial="0")

    #def clean_validators(self):
        #data  = self.cleaned_data['validators']
        #return ",".join( [str(v) for v in data] )

    def clean_answer_type(self):
        data = self.cleaned_data['answer_type']
        if data == "paypal" and not settings.PAYPAL_REDIRECT_HOSTNAME:
            raise forms.ValidationError( "Invalid Answer-Type" )
        return data

    def clean(self):
        cleaned_data = super(QuestionForm, self).clean()
        min_val      = cleaned_data.get("minimum")
        max_val      = cleaned_data.get("maximum")
        if all([ min_val, max_val ]) and min_val > max_val:
            raise forms.ValidationError("Minimum must be less than maximum")



#############################################
class Feedbacks(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Feedbacks"
        ordering            = ["project", "-timestamp"]

    project          = models.ForeignKey('Projects', on_delete=models.PROTECT)
    payment_received = models.BooleanField( default=False )
    timestamp        = models.DateTimeField(auto_now_add=True, db_index=True)
    username         = models.CharField(max_length=32,  blank=True, default="")
    user_agent       = models.CharField(max_length=255, blank=True, default="")

    def __unicode__(self):
        return "{}: {:%Y-%m-%d %H:%M}".format(self.project, self.timestamp)

    def answers(self, answer=""):
        return self.answers_set.filter(answer = answer)



#############################################
class Answers(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Answers"
        ordering            = ["project", "feedback", "question__question_no"]

    project       = models.ForeignKey('Projects' , on_delete=models.PROTECT)
    feedback      = models.ForeignKey('Feedbacks', on_delete=models.PROTECT)
    question      = models.ForeignKey('Questions', on_delete=models.PROTECT)
    answer        = models.TextField()

    def __unicode__(self):
        return "%s :: %s :: %s" % (self.feedback, self.question, self.answer )



#############################################
class AnswerEdits(MyBaseModel):
#############################################

    class Meta:
        verbose_name_plural = "Answer Edits"
        ordering            = ["-timestamp"]

    answer        = models.ForeignKey( 'Answers', null=True, on_delete=models.SET_NULL )
    user          = models.ForeignKey( User     , null=True, on_delete=models.SET_NULL )
    reason        = models.TextField()
    old_answer    = models.TextField( blank=True )
    new_answer    = models.TextField()
    timestamp     = models.DateTimeField( blank=True, auto_now_add=True )

    def __unicode__(self):
        return "%s : %s : %s" % ( self.reason, self.old_answer, self.new_answer )


#----------------------------
class AnswerEditForm(forms.ModelForm):
#----------------------------
    class Meta:
        model   = AnswerEdits
        exclude = []



#############################################
class ApplicationErrorLog( models.Model ):
#############################################

    class Meta:
        ordering = ["-timestamp"]

    project       = models.ForeignKey( 'Projects' )
    error         = models.CharField( max_length=255 )
    question_no   = models.IntegerField( null=True )
    answer        = models.TextField(    null=True )
    request       = models.TextField(    null=True )
    timestamp     = models.DateTimeField( blank=True, auto_now_add=True )

    def __unicode__(self):
        return "%s : %s : %s" % ( self.error, self.question_no, self.timestamp )
