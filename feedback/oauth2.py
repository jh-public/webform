from   pdb          import set_trace as st
import requests

# TOKEN_URL       = 'http://localhost/o/token/'
CLIENT_ID       = "wobMB9EzeITeOqJbrpcVMuFV64gM994yWyBDgr2o"
CLIENT_SECRET   = "kKllNruGNhzvjevouN4xywLJuVqnTOV8Fgnd8N6HEUUNe7grxTnllVoV8LYY91nMYhy4YR0gVh7xSMPRIvkvzqlXz9smTpxr5nIr28oYELuRMI0FeTuzjYMDnrv6SWWZ"
# USERNAME        = "admin"
# PASSWORD        = "xxx123"
# GRANT_TYPE      = "password"
# SCOPE           = "read"

class OAuth2( object ):

    def __init__(
        self,
        access_token_url,
        username,
        password,
        client_id = CLIENT_ID,
        client_secret = CLIENT_SECRET,
        scope = "read",
        grant_type = "password",
    ):
        self.access_token_payload   = locals()
        self.access_token_url       = self.access_token_payload.pop( "access_token_url" )
        self.access_token_response  = requests.post(
            url  = self.access_token_url,
            data = self.access_token_payload,
        )
        try:
            self.access_token_data = self.access_token_response.json()
            self.access_token      = self.access_token_data.get( "access_token" )
            self.error             = None
        except:
            self.error = self.access_token_response


    def get( self, api_url ):
        headers = {
            "Authorization": "Bearer %s" % self.access_token,
        }
        return requests.get(
            url     = api_url,
            headers = headers,
        )
