from rest_framework  import serializers
from feedback.models import *

class FormSerializer( serializers.ModelSerializer ):
    class Meta:
        model = Projects
        fields = ( 'reference', 'description', )
