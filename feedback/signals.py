from    django.dispatch                 import receiver
from    paypal.standard.models          import ST_PP_COMPLETED
from    paypal.standard.ipn.signals     import valid_ipn_received
import  models
import  pdb
import  sys

@receiver( valid_ipn_received )
def paypal_notification(sender, **kwargs):
    try:
        ipn_obj = sender
        if ipn_obj.payment_status == ST_PP_COMPLETED:
            invoice = ipn_obj.invoice.split("#")
            feedback_pk = int( invoice[-1] )
            feedback = models.Feedbacks.objects.get( pk=feedback_pk )
            feedback.payment_received = True
            feedback.save()
    except:
        pass
