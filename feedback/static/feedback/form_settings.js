'use strict';

angular.module('forms', [
    'djangoData'
    ,'ncc.directives'
    ,'validators'
    ,'colorPicker'
])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller( "feedback.formEditCtrl", [
    "$scope"
    ,"$timeout"
    ,"djangoData"
    ,'validators'
    ,function(
        $scope
        ,$timeout
        ,djangoData
        ,validators
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.django[k] = v;
        });

        var preReorder;
        $scope.tab          = ($scope.django.project && $scope.django.form_valid )? "questions" : "settings";
        $scope.reordering   = false;
        $scope.draggable    = undefined;
        $scope.modalInput   = "";
        $scope.modalTitle   = "";

        $scope.showColorModal = function( inputName, title ) {
            $scope.modalInput = inputName;
            $scope.modalTitle = "Choose " + title;
            $timeout( function() {
                $("#colorModal").modal( "show" );
            }, 0, false );
        };

        $scope.updateColor = function( color ) {
            $timeout( function(){
                document.forms.form1[ $scope.modalInput ].value = color;
                $("#colorModal").modal( "hide" );
            }, 0 , false );

        };

        $scope.reorder      = function(state) {
            $scope.reordering = state;
            if (state)  {
                $scope.draggable = "true";
                preReorder       = angular.copy($scope.django.questions);
            } else {
                $scope.draggable = undefined;
            }
        }
        $scope.reorderCancel = function(state) {
            $scope.reorder(false);
            $scope.django.questions = preReorder;
        };
        $scope.reorderDone  = function(state) {
            $scope.reorder(false);
            var order = [];
            angular.forEach( $scope.django.questions, function(v, i) {
                order.push(v.pk);
            });
            $("*[name=json_data]").val( JSON.stringify(order) );
            $("#form-reorder").submit();
        };

        $scope.getValidatorNames = function(question) {
            var qvalidators      = question.fields.validators.split(",");
            var validatorNames   = [];
            angular.forEach( qvalidators, function(v) {
                validatorNames.push( $scope.django.validators[v][1] );
            });
            return "Validations: " + validatorNames.join(", ");
        };

        $scope.changeTab = function(tab) {
            $scope.tab = tab;
        };

        function trim(str) {
            return str.replace(/^\s+|\s+$/gm, '');
        }
        $scope.chkEmail = function() {
            var elem = $("*[name=notify]")[0];
            elem.setCustomValidity("");
            var val  = elem.value;
            if (!val) return;
            var emails   = val.split("\n");
            var invalids = [];
            angular.forEach( emails, function(email) {
                var email = trim(email);
                if (!email) return;
                if ( !validators.Email(email) ) {
                    invalids.push(email);
                    return;
                }
            });
            if (invalids.length) {
                elem.setCustomValidity( "Invalid email(s): " + invalids.join(", ") );
                return false;
            }
            return true;
        };

        $scope.submit = function() {
            $scope.chkEmail() && $("#form1").submit();
        };

        $scope.viewTheme = function ( url ){
            var field = document.forms.form1.template;
            var cf    = document.forms.form1.color_font.value;
            var cq    = document.forms.form1.color_question.value;
            var ca    = document.forms.form1.color_accent.value;
            var theme = field.selectedOptions[0].value;
            url = url || "/test-theme/";
            url = ( url.trim()
                + "?template=" + encodeURIComponent( theme )
                + "&cf=" + encodeURIComponent( cf )
                + "&cq=" + encodeURIComponent( cq )
                + "&ca=" + encodeURIComponent( ca )
            );
            window.open( url );
        }
    }
])



.directive( "gnDragNDrop", [ function() {
    return {
        restrict:   "A",
        link: function( $scope, element, attrs ) {
            var element = $(element);
            element.on( "dragstart", function(e) {
                if ( e.target.tagName == "TD" ) {
                    e.originalEvent.dataTransfer.setData( "text", e.target.getAttribute("question_idx") );
                }
            });
            element.on( "dragover", function(e) {
                e.preventDefault();
                $(e.target).closest("tr")[0].classList.toggle("dragover", true);
            });
            element.on( "dragleave", function(e) {
                e.preventDefault();
                $(e.target).closest("tr")[0].classList.toggle("dragover", false);
            });
            element.on( "drop", function(e) {
                e.preventDefault();
                $(e.target).closest("tr")[0].classList.toggle("dragover", false);
                var from = parseInt( e.originalEvent.dataTransfer.getData("text") );
                var to   = parseInt( $(e.target).closest("td")[0].getAttribute("question_idx") );
                $scope.django.questions.splice( to, 0, $scope.django.questions[from] );
                if (from < to ) $scope.django.questions.splice( from,     1 );
                else            $scope.django.questions.splice( from + 1, 1 );
                $scope.$apply();
            });
        },
    };
}])



.directive( "gnCopyQuestions", [ '$http', function( $http ) {
    return {
        restrict:       "E",
        templateUrl:    "copyQuestions.html",
        controllerAs:   "copyQuestionsCtrl",
        controller:     [ '$scope', function( $scope ) {
            var modal = $("#modal-gn-copy-questions");

            this.copyQuestions = function() {
                $http.get("/forms/formlist.json/").success( function(data) {
                    this.forms = data;
                }.bind(this));

                modal.modal('show');
            }; // this.copyQuestions

            this.selectForm = function(form) {
                    console.log(this.form_id, $("*[name=form_id]").val() )
                    modal.modal('hide');
                    $("#form-gn-copy-questions").submit();
            }; // this.selectForm
        }], // controller
    }; // DDO
}])



.filter( "stripTags", function() {
    return function(input) {
        var txt = input;
        txt = txt.replace( /<\w+:.+?\/\s*?>/gm, "" );            // namespaced singleton tags
        txt = txt.replace( /<\w+:.+?>.+?>/gm,   "" );            // namespaced tags & contents
        txt = txt.replace( /<style>[\s\S]*?<\/style>/gm, "" );   // style tags & contents
        txt = txt.replace( /<([^>]+>)/gm, "");                   // remove all tags
        txt = txt.trim();
        if ( txt.length > 100 ) txt = txt.substr(0,100) + "...";
        return txt;
    };
})
