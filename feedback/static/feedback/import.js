'use strict';

angular.module('forms.import', ['djangoData', 'ncc.directives'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';    }
])


.controller( "importController", [
    "$scope",
    "$http",
    "$timeout",
    "djangoData",
    function(
        $scope,
        $http,
        $timeout,
        djangoData
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.django[k] = v;
        });

        $scope.imported = false;

        this.submit =function() {
            if ( $scope.imported )  return;

            if (!this.host)      return;
            if (!this.reference) return;
            var token   = $("*[name=csrfmiddlewaretoken]").val();
            var url     = this.host + "/forms/admin/export/" + this.reference + "/?callback=JSON_CALLBACK";

            $http.jsonp(url).then(

                function( data ) {
                    this.data = data;
                    debugger
                    $timeout( function(){
                        $("#form1").submit();
                    },0 , false);
                }.bind(this)

                ,function( data, status, headers, config ) {
                    debugger
                    this.errorMessage = "Status: " + status;
                }.bind(this)

            )

            $scope.imported = true;
        };

    }
])

