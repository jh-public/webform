'use strict';

angular.module( 'colorPicker', [ 'colors' ])

.component( "colorPicker", {
        templateUrl: "/static/partials/colorPicker.html"
        ,bindings: {
            modalTitle:     "<"
            ,update:        "&"
        }
        ,controller: [
            "colors"
        ,function(
            colors
        ){
            var baseColors  = [];
            var colorsLists = {};
            var prevclr     = null;
            angular.forEach( colors, function( v ) {
                var bclr = v[0]
                var clr  = v[1]
                var hex  = v[2]
                if ( prevclr != bclr )  baseColors.push( bclr );
                colorsLists[ bclr ] = colorsLists[ bclr ] || [];
                colorsLists[ bclr ].push([ clr, hex ]);
                prevclr = bclr;
            });
            this.colors = baseColors.map( function( clr ){
                return colorsLists[ clr ];
            });
        }
]})

