from    celery                          import task
from    django.core.mail                import send_mail, EmailMessage
from    pdb                             import set_trace as st
import  feedback.models as models


def error( errno=0, errmsg="", qno=0, answer="", feedback=0 ):
    return dict(
        error    = errno,
        message  = errmsg,
        question = qno,
        answer   = answer,
        feedback = feedback,
    )


def log_error( project, error, question_no, answer, request ):
    return models.ApplicationErrorLog.objects.create(
        project     = project,
        error       = str(error),
        question_no = question_no,
        answer      = answer,
        request     = str(request),
    )


#------------------------------------------------
@task()
def pre_processing( request, reference ):
#------------------------------------------------

    project = models.Projects.get( reference )

    # max feedbacks check
    if project.max_feedbacks and project.feedbacks_set.count() >= project.max_feedbacks:
        return error( 1, project.max_feedbacks_text or "The maximum number of responses has been reached." )

    # process answers
    questions = project.questions_set.all()
    bulk_answers  = []
    answer_dict   = request["answer_dict"]
    for question in questions:
        question_no = question.question_no
        answer      = ",".join( answer_dict[ "Q%s" % question_no ] )

        if question.answer_type not in [ "label", "readonly", "hidden" ]:

            if answer:
                if question.depends_on and question.depends_value not in answer_dict[ "Q%d" % question.depends_on.question_no ]:
                    log = log_error( project=project, error=21, question_no=question_no, answer=answer, request=request )
                    return error(
                        21,
                        "Application error 21: invalid data (Q%d:%d).  Please contact technical support." % ( question_no, log.pk ),
                        question_no,
                        answer,
                    )

                # Unique value validation
                validators = question.validators.split(",")
                if "1" in validators:
                    answers_qs = question.answers_set.filter( answer__iexact=answer )
                    if answers_qs.count():
                        return error(
                            2,
                            question.msg_err_unique or "Duplicate value - %s" % answer,
                            question_no,
                            answer,
                        )

                # Answer limit check
                if question.choice_category is not None:
                    choices = question.choice_category.choices_set.filter( name=answer )
                    limit   = choices[0].responses_limit if choices else 0
                    if limit:
                        responses = question.answers_set.filter( answer=answer )
                        if responses.count() >= limit:
                            return error(
                                3,
                                "Due to a high take-up rate, your selection %s is now fully subscribed. "
                                    "Please select another option. Thank you!"
                                    % answer,
                                question_no,
                                answer,
                            )

            # Answer required check
            elif question.required:

                if not question.depends_on or question.depends_value in answer_dict[ "Q%d" % question.depends_on.question_no ]:
                    log = log_error( project=project, error=22, question_no=question_no, answer=answer, request=request )
                    return error(
                        22,
                        "Application error 22: invalid data (Q%d:%d).  Please contact technical support." % ( question_no, log.pk ),
                        question_no,
                        answer,
                    )

        bulk_answers.append( dict(
            project  = project,
            question = question,
            answer   = answer,
        ))

    feedback            = models.Feedbacks(project=project)
    feedback.username   = request["username"]
    feedback.user_agent = request["user_agent"]
    feedback.save()
    models.Answers.objects.bulk_create( [ models.Answers( feedback = feedback, **answer ) for answer in bulk_answers ] )
    return error(feedback=feedback.pk)



#------------------------------------------------
@task()
def post_processing( data ):
#------------------------------------------------

    # feedback notification
    feedback_notification = data["feedback_notification"]
    if feedback_notification:
        send_mail( fail_silently = True, **feedback_notification )

    # user notification
    user_notification = data["user_notification"]
    for notification in user_notification:
        EmailMessage( **notification ).send( fail_silently=True )

