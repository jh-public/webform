from   django.conf.urls                 import include, url
from   django.contrib.auth.decorators   import login_required
from   django.http                      import HttpResponseRedirect
from   django.views.generic.base        import RedirectView
import views
import signals

app_name = 'forms'

urlpatterns = [
    url(r'^$',                                              lambda x: HttpResponseRedirect( "admin/" ) ),

    url(r'^admin/$',                                        login_required( views.ProjectList.as_view()        ),  name="project_list" ),
    url(r'^admin/import/$',                                 login_required( views.ImportProject.as_view()      ),  name="import" ),
    url(r'^admin/export/(.+)/$',                            login_required( views.ExportProject.as_view()      ),  name="export" ),
    url(r'^admin/newform/$',                                login_required( views.NewEditProject.as_view()     ),  name="new_project" ),
    url(r'^admin/editform/(.+)/reorder/$',                  login_required( views.ReorderQuestions.as_view()   ),  name="reorder_questions" ),
    url(r'^admin/editform/(.+)/delete/$',                   login_required( views.DeleteProject.as_view()      ),  name="delete_project" ),
    url(r'^admin/editform/(.+)/clear/$',                    login_required( views.ClearProject.as_view()       ),  name="clear_project" ),
    url(r'^admin/editform/(.+)/copyquestions/$',            login_required( views.CopyQuestions.as_view()      ),  name="copy_questions" ),
    url(r'^admin/editform/(.+)/$',                          login_required( views.NewEditProject.as_view()     ),  name="edit_project" ),

    url(r'^admin/newquestion/(.+)/$',                       login_required( views.NewEditQuestion.as_view()    ),  name="new_question" ),
    url(r'^admin/editquestion/(.+)/(.+)/delete/$',          login_required( views.DeleteQuestion.as_view()     ),  name="delete_question" ),
    url(r'^admin/editquestion/(.+)/(.+)/$',                 login_required( views.NewEditQuestion.as_view()    ),  name="edit_question" ),
    url(r'^admin/editquestion/$',                           login_required( views.NewEditQuestion.as_view()    ),  name="edit_question_url_prefix" ),

    url(r'^admin/answers/.+?/(.+?)/$',                      login_required( views.AnswersEdit.as_view()        ),  name="answers_edit" ),
    url(r'^admin/answers/(.+?)/$',                          login_required( views.AnswersList.as_view()        ),  name="answers_list" ),
    url(r'^admin/answers/$',                                login_required( views.AnswersFormList.as_view()    ),  name="answers_form_list" ),

    url(r'^admin/data/(.+)/(summary|list)/?(csv)?/$',       login_required( views.FeedbackData.as_view()       ),  name="feedback_data" ),
    url(r'^admin/data/(.+)/answer/$',                       login_required( views.FeedbackAnswers.as_view()    ),  name="feedback_answers" ),
    url(r'^chart(/(?P<pk>.+))?(/(?P<charttype>.+))?/$',     login_required( views.Chart.as_view()              ),  name="feedback_chart" ),

    url(r'^formlist.json/$',                                login_required( views.FormList.as_view()           ),  name="form_list" ),
    url(r'^unique/reference/(.+)$',                         login_required( views.UniqueReference.as_view()    ),  name="unique_reference" ),

    url(r'^unique/$',                                       views.unique,                                          name="unique" ),

    url(r'^(.+)/closed/$',                                  views.thanks,              {"template":"closed.html"}, name="closed" ),
    url(r'^(.+)/thanks/$',                                  views.thanks,              {"template":"thanks.html"}, name="thanks" ),
    url(r'^(.+)/maxed/(?:(.+)/)?$',                         views.Maxed.as_view(),                                 name="maxed" ),
    url(r'^(.+)/duplicate/(?:(.+)?)/$',                     views.Duplicate.as_view(),                             name="duplicate" ),

    url(r'^(.+)/$',                                         views.Questionnaire.as_view(),                         name="questionnaire" ),
]
