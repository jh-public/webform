from    __future__                          import print_function
from    django.core                         import serializers, urlresolvers
from    django.core.paginator               import Paginator, EmptyPage, PageNotAnInteger
from    django.db.models                    import Q, ProtectedError
from    django.forms.models                 import inlineformset_factory
from    django.http                         import HttpResponse, HttpResponseRedirect, Http404
from    django.shortcuts                    import render
from    django.template                     import loader, TemplateDoesNotExist, Template, Context
from    django.utils.timezone               import localtime
from    django.views.decorators.cache       import never_cache
from    django.views.decorators.csrf        import csrf_exempt
from    django.views.generic                import View
from    django.views.generic.base           import TemplateView
from    django.utils.html                   import strip_tags
from    paypal.standard.forms               import PayPalPaymentsForm

from    feedback                            import tasks, oauth2
from    project                             import date_handler, json_dumps, settings

import  csv
import  collections
import  datetime
import  django
import  itertools
import  json
import  logging
import  models
import  operator
import  pytz
import  re
import  requests
import  StringIO
import  time

from    pdb import set_trace as st


#################################################
def get_date_range( request ):
#################################################
    date_to   = request.GET.get("to")
    if date_to:   date_to   = datetime.datetime.combine( datetime.datetime.strptime(date_to,   "%d-%b-%Y"), datetime.time(23,59) )
    else:         date_to   = datetime.datetime.combine( datetime.date.today(), datetime.time(23,59) )

    date_from = request.GET.get("from")
    if date_from: date_from = datetime.datetime.strptime(date_from, "%d-%b-%Y")
    else:         date_from = date_to - datetime.timedelta(days=10)

    return date_from, date_to



#################################################
def get_counts(question, date_from=None, date_to=None):
#################################################
    if question.answer_type == "checkbox":
        data = [
            [ "Yes", question.answer_value("yes", date_from, date_to).count() ],
            [ "No",  question.answer_value("no" , date_from, date_to).count() ],
        ]
    elif question.answer_type == "radio":
        if question.choice_category:
            data = [
                [ option.name, question.answer_value(option.name, date_from, date_to).count() ]
                for option in question.choice_category.choices_set.all()
            ]
        else:
            data = [
                [ option, question.answer_value(option, date_from, date_to).count() ]
                for option in question.options_split()
            ]
        blanks  = question.answer_blanks(date_from, date_to).count()
        if blanks: data.append([ "Blank",  blanks ])
    else:
        data    = [
            [ "Non-blank",      question.answer_nonblanks(date_from, date_to).count() ],
            [ "Blank",          question.answer_blanks(date_from, date_to).count()    ],
        ]
    return data



#################################################
def serialize( queryset, format="json", **kwargs ):
#################################################
    return serializers.serialize( format, queryset, **kwargs )



#################################################
def message( request, msg, msg_class="danger" ):
#################################################
    return render(
        request,
        'message.html',
        {
            "message":   msg,
            "msg_class": msg_class,
        },
    )



#################################################
class DirectTemplateView(TemplateView):
#################################################
    extra_context = None
    def get_context_data(self, **kwargs):
        context = super(self.__class__, self).get_context_data(**kwargs)
        if self.extra_context is not None:
            for key, value in self.extra_context.items():
                if callable(value):
                    context[key] = value()
                else:
                    context[key] = value
        return context



#################################################
@csrf_exempt
def thanks( request, reference=None, template=None ):
#################################################

    if reference:
        project      = models.Projects.get(reference)
        title        = project.description

        if project.redirect_text.strip():
            msg_template = Template( project.redirect_text )
            context      = Context( { "answer": request.session.get("answer_dict", {} ) })
            text_msg     = msg_template.render( context )
        else:
            text_msg = "Thank you for your feedback."

    return render( request, template, locals() )



#################################################
class Duplicate(View):
#################################################

    def get(self, request, project_id, question_id=None, errmsg_field=None):
        if project_id:      project  = models.Projects.get( project_id )
        if question_id:     question = models.Questions.objects.get( pk=question_id )
        #if errmsg_field:    errmsg   = getattr( question, errmsg_field )
        template = "duplicate.html"
        return render( request, template, locals() )



#################################################
class Maxed(View):
#################################################

    def get(self, request, project_id, answer_id=None):
        if project_id:      project = models.Projects.get( project_id )
        if answer_id:       answer  = models.Answers.objects.get( pk=answer_id )
        template = "maxed.html"
        return render( request, template, locals() )



#################################################
class ProjectList(View):
#################################################

    TEMPLATE = 'form_list.html'
    TITLE    = lambda self, request: "%s %s" % ( request.site.name, "Forms" )

    def get(self, request):
        project_qs  = models.Projects.objects.filter(status__gte=0)
        projects    = [p for p in project_qs.values()]
        user_groups = request.user.groups.order_by( "name" )
        today       = datetime.date.today()
        orderBy     = request.GET.get( "o", "description" )

        for project, pobj in zip( projects, project_qs ):
            project["feedback_count"] = models.Feedbacks.objects.filter(project=pobj).count()
            project["readable"]       = any([ pobj.can_read (g) for g in user_groups ]) or request.user.is_superuser
            project["writable"]       = any([ pobj.can_write(g) for g in user_groups ]) or request.user.is_superuser
            project["active"]         = "Active" if not pobj.expiry else "Active" if pobj.expiry >= today else "Expired"
            project["expiry"]         = project["expiry"].isoformat() if project["expiry"] else ""
            try:
                project["latest"] = pobj.feedbacks_set.latest( "timestamp" ).timestamp.isoformat()
            except models.Feedbacks.DoesNotExist:
                pass

        projects = [ project for project in projects if project["readable"] or project["writable"] ]

        angularData = json.dumps(
            {
                "projects":projects,
                "groups": [ { "group_id": g.pk, "name": g.name } for g in user_groups ],
                "orderBy": orderBy,
            },
            indent=4,
            default=date_handler )

        title    = self.TITLE( request )
        template = self.TEMPLATE
        return render( request, template, locals() )



#################################################
class NewEditProject(View):
#################################################

    TEMPLATE = 'form_settings.html'
    TITLE    = lambda self, request: "%s %s" % ( request.site.name, "Forms" )

    def get_angular_data(self, request, project, form_valid=True):
        user_ministry_groups = list( models.get_user_ministry_groups(request.user) )
        if project is None:
            groups_read  = [ (group.pk, group.name, i==0) for i, group in enumerate( user_ministry_groups ) ]
            groups_write = groups_read
        else:
            groups_read  = [ ( group.pk, group.name, project.can_read (group) ) for group in user_ministry_groups ]
            groups_write = [ ( group.pk, group.name, project.can_write(group) ) for group in user_ministry_groups ]

        questions        = project.questions_set.order_by( "question_no" ) if project else []

        return json.dumps({
            "groups_read"               : groups_read,
            "groups_write"              : groups_write,
            "project"                   : project.model_to_dict() if project else "",
            "questions"                 : serialize( questions, format="python", use_natural_foreign_keys=True ),
            "edit_question_url_prefix"  : urlresolvers.reverse("forms:edit_question_url_prefix"),
            "validators"                : models.Questions.VALIDATORS,
            "form_valid"                : form_valid,
        }, indent=4, default=date_handler )


    #-------------------
    def get(self, request, pk=None):
    #-------------------
        project = models.Projects.objects.get( pk=int(pk), status__gte=0 ) if pk else None
        if pk and not project: raise Http404
        if pk and not models.access_granted(request.user, project.perms_write):
            return message( request, "You do not have permission to edit this form. Please contact the owner of the form." )
        form        = models.ProjectForm(instance=project, user=request.user)
        validators  = models.Questions.VALIDATORS
        angularData = self.get_angular_data(request, project)
        form_valid  = True
        title       = "%s: %s" % ( self.TITLE( request ), project.description if project else " (new)" )
        return render( request, self.TEMPLATE, locals() )


    #-------------------
    def post(self, request, pk=None):
    #-------------------

        def get_date(strdate):
            if not strdate:         return strdate
            try:
                return datetime.datetime.strptime(strdate, "%d-%b-%Y")
            except ValueError:
                return datetime.datetime.strptime(strdate, "%d-%b-%Y %H:%M").date()

        if pk is None and not request.user.has_perm("feedback.add_projects"):  raise Http404
        project = models.Projects.objects.get( pk=int(pk), status__gte=0 ) if pk else models.Projects()
        if pk:
            if not project or not models.access_granted(request.user, project.perms_write):
                raise Http404

        post = request.POST.copy()
        post["expiry"] = get_date( post["expiry"] )

        form = models.ProjectForm(post, request.FILES, instance=project, user=request.user)

        form_valid = form.is_valid()
        if form_valid:
            p = form.save()
            if pk: return HttpResponseRedirect( urlresolvers.reverse("forms:project_list") )
            else:  return HttpResponseRedirect( urlresolvers.reverse("forms:edit_project", args=[p.pk]) )

        validators  = models.Questions.VALIDATORS
        angularData = self.get_angular_data(request, project, form_valid=form_valid)
        title       = self.TITLE( request )

        return render( request, self.TEMPLATE, locals() )



#################################################
class ImportProject(View):
#################################################

    def get(self, request):
        template = 'import.html'
        return render( request, template, locals() )


    def post(self, request):
        host            = request.POST[ "host" ]
        reference       = request.POST[ "reference" ]
        username        = request.POST[ "username" ]
        password        = request.POST[ "password" ]
        client_id       = request.POST[ "client_id" ]
        client_secret   = request.POST[ "client_secret" ]
        host            = host.strip()
        host            = host[:-1] if host.endswith("/") else host
        api_url         = "%s/api/forms/%s/" % ( host, reference )

        token = oauth2.OAuth2(
            access_token_url = "%s/o/token/" % host,
            username         = username,
            password         = password,
            client_id        = client_id,
            client_secret    = client_secret,
            scope            = "forms",
        )

        if token.error is not None:
            return HttpResponse( token.error.text )

        response = token.get( api_url )
        try:
            project = response.json()
        except:
            return HttpResponse( response.text )

        try:
            questions = project.pop( "questions_set" )
        except:
            return HttpResponse( response.text )

        project.pop("id")
        project["status"]     = project["status"] or 0
        project["reference"]  = "%s-%s" % ( project["reference"], str( int( time.time() )))
        project["created_by"] = request.user
        project = models.Projects.objects.create(**project)

        qdict = {}
        for question in questions:
            cc = question["choice_category"]
            choice_category = cc
            if cc:
                cc_name = cc["name"]
                try:
                    choice_category = models.ChoiceCategories.objects.get( name = cc_name )
                except ChoiceCategories.DoesNotExist:
                    choice_category = models.ChoiceCategories( name = cc_name ).save()

                    for choice in cc[ "choices_set" ]:
                        choice[ "choice_category" ] = choice_category
                        models.Choices( **choice ).save()

            qno                         = question["question_no"]
            question["project"]         = project
            question["depends_on"]      = qdict.get( qno )
            question["choice_category"] = choice_category
            new_q = models.Questions( **question )
            new_q.save()
            qdict[ qno ] = new_q

        return HttpResponseRedirect( urlresolvers.reverse("forms:project_list") )



#################################################
class ExportProject(View):
#################################################

    #-------------------
    def get(self, request, reference):
    #-------------------
        project   = models.Projects.get(reference)
        questions = project.questions_set.order_by('question_no')
        choice_categories = {}
        for question in questions:
            if question.choice_category:
                choice_categories[ question.pk ] = question.choice_category
        questions = serialize( questions, format="python", use_natural_foreign_keys=True )
        for question in questions:
            cc = choice_categories.get( question["pk"] )
            question["fields"]["choice_category"] = cc.name if cc else None
        callback  = request.GET["callback"]
        st()
        data      = {
            "project":      project.model_to_dict(),
            "questions":    questions,
            "choices":      serialize(
                models.Choices.objects.filter( choice_category__in = choice_categories.values() ),
                format="python",
                )
            }
        response = "%s ( %s )" % ( callback, json_dumps(data) )
        return HttpResponse( response, content_type="application/json" )



#################################################
class DeleteProject(View):
#################################################

    def post(self, request, pk):
        project = models.Projects.objects.get( pk=int(pk) )
        if not models.access_granted(request.user, project.perms_write):  raise Http404
        project.status    = -1
        project.reference = "%s-%s" % ( project.reference, str( int( time.time() )))
        project.save()
        return HttpResponseRedirect( urlresolvers.reverse("forms:project_list") )



#################################################
class ClearProject(View):
#################################################

    def post(self, request, pk):
        project = models.Projects.objects.get( pk=int(pk) )
        if not request.user.is_superuser:  raise Http404
        project.answers_set.all().delete()
        project.feedbacks_set.all().delete()
        return HttpResponseRedirect( urlresolvers.reverse("forms:project_list") )



#################################################
class FormList(View):
#################################################

    #-------------------
    def get(self, request):
    #-------------------
        data =  list( models.Projects.objects.values('id', 'description', 'status') )
        return HttpResponse( json.dumps(data), content_type="application/json" )


#################################################
class CopyQuestions(View):
#################################################

    #-------------------
    def post(self, request, pk ):
    #-------------------
        project = models.Projects.objects.get( pk=int(pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write):  raise Http404
        lastqno = 0
        lastq   = project.questions_set.order_by('-question_no')[:1]
        if lastq.count():  lastqno = lastq[0].question_no

        project_from = models.Projects.objects.get( pk=request.POST["form_id"] )
        qdict = {}
        for question in project_from.questions_set.order_by('question_no'):

            question.pk = None
            question.id = None
            question.save()
            question.project            = project
            qdict[question.question_no] = question
            lastqno                    += 1
            question.question_no        = lastqno

            if question.depends_on:
                depends_on_qno      = question.depends_on.question_no
                question.depends_on = qdict.get(depends_on_qno)

            if question.depends_on is None:
                question.depends_value = ""

            question.save()

        return HttpResponseRedirect( urlresolvers.reverse("forms:edit_project", args=[pk]) )



#################################################
class ReorderQuestions(View):
#################################################

    def post(self, request, pk):
        project = models.Projects.objects.get( pk=int(pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write):  raise Http404

        pks       = json.loads( request.POST["json_data"] )
        questions = models.Questions.objects.in_bulk(pks)

        for i, pk in enumerate(pks, 1):
            question = questions[pk]
            if question.question_no != i:
                question.question_no = i
                question.save()

        return HttpResponseRedirect( request.META["HTTP_REFERER"] )



#################################################
class NewEditQuestion(View):
#################################################

    TEMPLATE = 'question.html'
    TITLE    = lambda self, request: "%s %s" % ( request.site.name, "Forms" )

    def get_angular_data(self, request, project, question):
        questions = project.questions_set.order_by( "question_no" ).filter( answer_type__in=[ "radio", "checkbox", "multicheckbox" ] )
        if question: questions = questions.filter( question_no__lt=question.question_no )
        choiceCategories = models.ChoiceCategories.objects.all()
        ser_questions    = serialize( questions, format="python" )
        for q, sq in zip( questions, ser_questions):
            sq = sq["fields"]
            if q.choice_category is not None:
                sq["options"] = "\n".join( [
                    c.name
                    for c in q.choice_category.choices_set.all()
                ] )
        return json.dumps({
            "question"          : question.model_to_dict() if question else {},
            "questions"         : ser_questions,
            "validators"        : models.Questions.VALIDATORS,
            "answer_types"      : models.Questions.ANSWER_TYPES,
            "options_columns"   : models.Questions.OPTIONS_COLUMNS,
            "choiceCategories"  : serialize(choiceCategories, format="python"),
        }, indent=4)


    #-------------------
    def get(self, request, project_pk, pk=None):
    #-------------------
        project = models.Projects.objects.get( pk=int(project_pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write):  raise Http404
        question     = models.Questions.objects.get( pk=int(pk) ) if pk else None
        initial_data = {}
        lastq        = project.questions_set.order_by( "-question_no" )[:1]
        initial_data["question_no"] = lastq[0].question_no + 1 if lastq.count() else 1
        if pk: form = models.QuestionForm(instance=question)
        else:  form = models.QuestionForm(instance=question, initial=initial_data)
        angularData = self.get_angular_data(request, project, question)
        title       = "%s: %s - Question #%s" % (
            self.TITLE( request ),
            project.description if project else " (new)",
            question.question_no if question else "(new)"
        )

        return render( request, self.TEMPLATE, locals() )


    #-------------------
    def post(self, request, project_pk, pk=None):
    #-------------------
        project = models.Projects.objects.get( pk=int(project_pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write):  raise Http404
        question = models.Questions.objects.get( pk=int(pk) ) if pk else None
        form     = models.QuestionForm( request.POST, request.FILES, instance=question )

        if form.is_valid():
            q = form.save( commit=False )
            if q.answer_type not in [ "radio", "multicheckbox" ]:
                q.choice_category = None
            q.save()

            return HttpResponseRedirect( urlresolvers.reverse("forms:edit_project", args=[project.id]) )

        angularData = self.get_angular_data(request, project, question)
        title       = "%s: %s - Question #%s" % (
            self.TITLE( request ),
            project.description if project else " (new)",
            question.question_no if question else "(new)",
        )

        return render( request, self.TEMPLATE, locals() )


#################################################
class DeleteQuestion(View):
#################################################

    def post(self, request, project_pk, pk):
        project = models.Projects.objects.get( pk=int(project_pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write):  raise Http404
        question = models.Questions.objects.get( pk=int(pk) )
        try:   question.delete()
        except ProtectedError as e:
            return message( request, "Error: Unable to delete question - there is form data in database." )
        return HttpResponseRedirect( urlresolvers.reverse("forms:edit_project", args=[project_pk]) )



#################################################
class FeedbackData(View):
#################################################

    TITLE = "Form Data"

    def csv(self, request, project, date_from, date_to):

        def get_group_columns( groups, answers ):
            empty_answer = models.Answers()
            answers_dict = { answer.question.question_no : answer for answer in answers }
            return [ "\n".join( answers_dict.get( qno, empty_answer ).answer.encode("utf-8") for qno in qnos if answers_dict.get( qno, empty_answer ).answer.strip() )
                    for group, qnos in sorted( groups.iteritems() )
            ]

        def get_groups( depends_on ):
            groups = collections.defaultdict( list )
            for qno, dq in depends_on.iteritems():
                level = 1
                while depends_on.get( dq.question_no ):
                    level += 1
                    dq = depends_on.get( dq.question_no )
                groups[ "Q%02d-L%d" % ( dq.question_no, level ) ].append( qno )
            return groups

        f          = StringIO.StringIO()
        writer     = csv.writer(f)
        tags       = re.compile( r"<[^>]+>" )

        questions       = project.questions_set.all()
        questions_dict  = { question.question_no : question for question in questions }
        qhdr            = ["Time"] + [ "%d. %s" % ( question.question_no, tags.sub( "", question.question ).encode("utf-8") ) for question in questions ]
        grp             = request.GET.get( "grp" )
        if grp:
            get_group_hdr   = lambda groups, questions_dict: [ "+ " + strip_tags( questions_dict[ qnos[0] ].question ) for k, qnos in sorted( groups.iteritems() ) ]
            depends_on      = { q.question_no : q.depends_on for q in questions if q.depends_on }
            groups          = get_groups( depends_on )
            qhdr           += get_group_hdr( groups, questions_dict )
        writer.writerow( qhdr )

        data =  []

        for feedback in project.feedbacks_set.filter( timestamp__range=(date_from, date_to) ).order_by("-timestamp"):
            answers = feedback.answers_set.all()
            tz      = pytz.timezone( 'Asia/Singapore' )

            d = (
                    [ feedback.timestamp.replace( tzinfo=tz ).strftime("%Y-%m-%d %H:%M") ] +
                    [ answer.answer.encode("utf-8") for answer in answers ]
            )
            if grp:
                d += get_group_columns( groups, answers )
            data.append( d )

        writer.writerows(data)
        response = HttpResponse( f.getvalue(), content_type="text/csv" )
        response['Content-Disposition'] = 'attachment; filename="data.csv"'
        return response


    #-------------------
    def summary(self, request, project, date_from, date_to):
    #-------------------
        questions = project.questions_set.filter(
            ~Q( answer_type__in = [ "readonly", "hidden", "label" ] )
        ).order_by("question_no")

        for q in questions: q.counts = get_counts(q, date_from, date_to)

        angularData = json_dumps({
            "dateFrom": date_from.strftime("%d-%b-%Y"),
            "dateTo":   date_to.strftime("%d-%b-%Y"),
        })

        title    = self.TITLE
        template = 'feedback_summary.html'
        return render( request, template, locals() )


    #-------------------
    def feedback_list(self, request, project, date_from, date_to):
    #-------------------
        feedbacks = project.feedbacks_set.filter( timestamp__range=(date_from, date_to) ).order_by("-timestamp")
        for fb in feedbacks:
            fb.answers = project.answers_set.filter( feedback = fb ).order_by('question__question_no')

        angularData = json_dumps({
            "dateFrom": date_from.strftime("%d-%b-%Y"),
            "dateTo":   date_to.strftime("%d-%b-%Y"),
        })

        title    = self.TITLE
        template = 'feedback_list.html'
        return render( request, template, locals() )


    #-------------------
    def get(self, request, pk, view_type, format=None):
    #-------------------
        date_from, date_to = get_date_range( request )
        project = models.Projects.objects.get(pk=int(pk))
        if not models.access_granted(request.user, project.perms_read):
            return message( request, "You do not have permission to view data collected for form. Please contact the owner of the form." )

        if format == "csv":
            return self.csv(request, project, date_from, date_to)
        if view_type == "summary":
            return self.summary(request, project, date_from, date_to)
        else:
            return self.feedback_list(request, project, date_from, date_to)



#################################################
class FeedbackAnswers(View):
#################################################

    TITLE = "Form Answer"

    #-------------------
    def get(self, request, pk):
    #-------------------
        feedback = models.Feedbacks.objects.get( pk=int(pk) )
        if not models.access_granted(request.user, feedback.project.perms_read):
            return message( request, "You do not have permission to view data collected for form. Please contact the owner of the form." )
        answers  = feedback.answers_set.order_by("question__question_no")
        title    = self.TITLE
        template = 'feedback_answers.html'
        return render( request, template, locals() )



#################################################
class Chart(View):
#################################################

    TITLE = "Form Data Chart"

    def pie(self, request, question):
        date_from, date_to  = get_date_range( request )
        charttype           = "pieChart"
        data                = get_counts(question, date_from, date_to)
        data                = sorted( data, key=operator.itemgetter(1), reverse=True )
        xdata, ydata        = zip(*data)
        cut                 = request.GET.get("cut")
        length              = int( request.GET.get("length", 0) )

        if cut: xdata = [ x.split(cut)[0] for x in xdata ]

        if length:
            xdata = [ x[:length] for x in xdata ]
        else:
            xdata = [ "%s..." % x[:50] if len(x) > 50 else x for x in xdata ]

        extra_serie  = {
            "tooltip": {
                "y_start": "",
                "y_end":   "",
            },
        }
        title           = self.TITLE
        chartdata       = {'x': xdata, 'y1': ydata, 'extra1': extra_serie}
        width           = request.GET.get("width",  350)
        height          = request.GET.get("height", 350)
        margin_top      = request.GET.get("margin_top",     30)
        margin_bottom   = request.GET.get("margin_bottom",  30)
        margin_left     = request.GET.get("margin_left",  60)
        margin_right    = request.GET.get("margin_right", 60)
        header          = request.GET.get("header", 1)
        fullscreen      = request.GET.get("f")

        kw_extra = {}
        kw_extra["height"]          = int(height)
        kw_extra["show_legend"]     = False
        kw_extra["margin_top"]      = margin_top
        kw_extra["margin_bottom"]   = margin_bottom
        kw_extra["margin_left"]     = margin_left
        kw_extra["margin_right"]    = margin_right

        return render(request, 'piechart.html', locals())


    #-------------------
    def get(self, request, pk, charttype):
    #-------------------
        question = models.Questions.objects.get( pk=int(pk) )
        if not models.access_granted(request.user, question.project.perms_read):
            return HttpResponse("You do not have permission to view data collected for form. Please contact the owner of the form.")
        return getattr(self, charttype or "pie")(request, question)



#################################################
@csrf_exempt
def unique(request):
#################################################
    data    = json.loads( request.body )
    project = int( data["project"] )
    qno     = int( data["qno"] )
    value   = data["value"]
    if not project: raise Http404
    if not qno:     raise Http404
    if not value:   raise Http404

    answers = models.Answers.objects.filter( project__pk=project, question__question_no=qno, answer__iexact=value )
    return HttpResponse( answers.count() )



#################################################
class UniqueReference(View):
#################################################

    def get(self, request, reference):
        project = models.Project.get(reference)
        return HttpResponse( answers.count() )



#################################################
class AnswersFormList( ProjectList ):
#################################################

    TEMPLATE = 'answers_formlist.html'
    TITLE    = "Answer Forms"



#################################################
class AnswersList(View):
#################################################

    TEMPLATE = 'answers_list.html'
    TITLE    = "Form Answers"

    #-------------------
    def get(self, request, pk):
    #-------------------
        project = models.Projects.objects.get( pk=int(pk), status__gte=0 )
        if not project: raise Http404
        if not models.access_granted(request.user, project.perms_write): raise Http404

        feedbacks = project.feedbacks_set.all()
        paginator = Paginator( feedbacks, 50 )

        page = request.GET.get('page')
        try:
            feedbacks = paginator.page(page)
        except PageNotAnInteger:
            feedbacks = paginator.page(1)
        except EmptyPage:
            feedbacks = paginator.page(paginator.num_pages)

        title = self.TITLE
        return render( request, self.TEMPLATE, locals() )



#################################################
class AnswersEdit(View):
#################################################

    TEMPLATE = 'answers_edit.html'
    TITLE    = "Edit Answer"

    #-------------------
    def get(self, request, pk ):
    #-------------------
        answer = models.Answers.objects.get( pk=int(pk) )
        if not answer: raise Http404
        if not models.access_granted(request.user, answer.project.perms_write): raise Http404
        form = models.AnswerEditForm( initial={
            "answer"      : answer,
            "user"        : request.user,
            "old_answer"  : answer.answer,
        })
        title = self.TITLE
        return render( request, self.TEMPLATE, locals() )


    #-------------------
    def post(self, request, pk ):
    #-------------------
        answer = models.Answers.objects.get( pk=int(pk) )
        if not answer: raise Http404
        if not models.access_granted(request.user, answer.project.perms_write): raise Http404

        form = models.AnswerEditForm( request.POST )
        if form.is_valid():
            data          = form.save()
            answer.answer = data.new_answer
            answer.save()
            return HttpResponseRedirect( urlresolvers.reverse("forms:answers_list", args=[answer.project.pk]) )

        title = self.TITLE
        return render( request, self.TEMPLATE, locals() )



#################################################
class Questionnaire(View):
#################################################

    BLANKS = {
        "checkbox": ["No"],
    }

    #--------------------------------------------
    @never_cache
    def get(self, request, reference):
    #--------------------------------------------
        project = models.Projects.get(reference)
        if not project: raise Http404
        if project.status < 0: raise Http404
        if not project.status and not request.user.username: raise Http404 # must be logged on if status is 0

        if project.expiry and project.expiry < datetime.datetime.today().date():
            if project.closure_url:
                return HttpResponseRedirect( project.closure_url )
            else:
                return HttpResponseRedirect( urlresolvers.reverse( "forms:closed", args=[reference] ) )

        if project.max_feedbacks and project.feedbacks_set.count() >= project.max_feedbacks:
            if project.max_feedbacks_url:
                return HttpResponseRedirect( project.max_feedbacks_url )
            else:
                return HttpResponseRedirect( urlresolvers.reverse( "forms:maxed", args=[reference] ) )

        # Unique value check
        for key, value in request.GET.items():
            if not key.startswith("Q"): continue
            try:   qno = int( key[1:] )
            except ValueError:  continue
            qs = models.Answers.objects.filter( project=project, question__question_no=qno, answer__iexact=value )
            if qs.count():
                question = qs[0].question
                errmsg  = question.msg_err_unique.format(value) if question.msg_err_unique else "Duplicate record error."
                value    = None
                return render( request, "duplicate.html", locals() )


        # questions
        questions     = project.questions_set.order_by("question_no")
        ser_questions = serialize( questions, format="python", use_natural_foreign_keys=True )
        for q, sq in zip( questions, ser_questions):
            sq = sq["fields"]
            if q.choice_category is not None:
                sq["options"] = "\n".join( [
                    c.name
                    for c in q.choice_category.choices_set.all()
                    if c.responses_limit == 0 or q.answers_set.filter( answer=c.name ).count() < c.responses_limit
                ] )

        # custom colors
        colors = {
            "color_font"      : request.GET.get( "cf" ) or project.color_font,
            "color_question"  : request.GET.get( "cq" ) or project.color_question,
            "color_accent"    : request.GET.get( "ca" ) or project.color_accent,
        }
        angularData = json_dumps({
            "project":      project.model_to_dict(),
            "questions":    ser_questions,
            "validators":   [v for _,v in models.Questions.VALIDATORS],
            "initial":      request.GET.dict(),
        })

        [ setattr( q, "answer", request.GET.get( str(q.question_no) )) for q in questions if str(q.question_no) in request.GET ]

        pages       = []
        qgroups     = []
        for q in questions:
            qgroups.append(q)
            if q.page_break:
                pages.append({ "questions": qgroups, "lastq": q.question_no })
                qgroups     = []
        if qgroups: pages.append({ "questions": qgroups, "lastq": q.question_no })

        template    = 'questionnaire/%s' % ( request.GET.get( "template" ) or project.template )
        title       = project.description
        ga_code     = project.ga_code or settings.GA_CODE
        frsc        = "".join( reversed( django.middleware.csrf.get_token( request ) ))
        nocache     = True
        return render( request, template, locals() )



    #--------------------------------------------
    def pre_processing( self, request, reference ):
    #--------------------------------------------

        def error( errno, errmsg="", qno=0 ):
            return json_dumps( dict(
                error    = errno,
                message  = errmsg,
                question = qno,
            ))

        project = models.Projects.get( reference )
        if not project: return error( 11, "Project not found." )
        if project.status < 0: return error( 12, "Project deleted." )
        if not project.status and not request.user.username: return error( 13, "Invalid user." )
        if project.expiry and project.expiry < datetime.datetime.today().date(): return error( 14, "Project expired." )

        questions   = project.questions_set.all()
        answer_dict = {}
        for question in questions:
            qno    = "Q%d" % question.question_no
            blank  = self.BLANKS.get( question.answer_type, [] )
            answer_dict[ qno ] = request.POST.getlist(qno, blank)

        request = dict(
            answer_dict = answer_dict,
            username    = request.user.username or "",
            user_agent  = request.META.get('HTTP_USER_AGENT'),
            meta        = dict( ( k, str(v) ) for k,v in request.META.iteritems() ),
        )

        # result = tasks.pre_processing( request, reference )  ### debug
        # return HttpResponse( json_dumps( result ))           ### debug

        # result = result.get()                                ### debug
        # json_d = json_dumps( result )                        ### debug
        # return json_d                                        ### debug

        result = tasks.pre_processing.apply_async( args=[ request, reference ], queue=project.queue or settings.DEFAULT_QUEUE )
        return HttpResponse( json_dumps( result.get() ))



    #--------------------------------------------
    def post_processing( self, request, reference, feedback_pk ):
    #--------------------------------------------

        project = models.Projects.get(reference)
        if not project: raise Http404
        if project.status < 0: raise Http404
        if not project.status and not request.user.username: raise Http404
        if project.expiry and project.expiry < datetime.datetime.today().date(): raise Http404
        if not feedback_pk:  raise Http404


        # feedback submission notification
        feedback_notification = None
        recipients = [e.strip() for e in project.notify.split("\n")]
        if any(recipients):
            url      = "http://%s%s" % (request.META["HTTP_HOST"], urlresolvers.reverse( "forms:feedback_answers", args=[feedback_pk] ) )
            text_msg = "Link to form data: %s." % url
            html_msg = "Link to form data: <a href='%(url)s'>%(url)s</a>." % {"url":url}
            feedback_notification = dict(
                subject         = 'New ' + project.description,
                message         = text_msg,
                from_email      = 'form_data@newcreation.org.sg',
                recipient_list  = recipients,
                html_message    = html_msg,
            )

        # user notification
        get_blank_val = lambda question: self.BLANKS.get( question.answer_type, [] )
        question_dict = { q.question_no: q for q in project.questions_set.all() }
        answer_dict   = {
            question.question_no:
            ",".join( request.POST.getlist( "Q%d" % question.question_no, get_blank_val( question) ))
            for question in question_dict.itervalues()
        }
        user_notification = []
        for email_qno in request.POST.getlist("email_send_confirmation"):
            email_qno = int( email_qno )
            email     = request.POST.get( "Q%d" % email_qno )
            if email:
                question = question_dict[ email_qno ]
                template = Template( question.email_confirmation_text )
                context  = Context( { "answer": answer_dict } )
                text_msg = template.render( context )
                cc       = question.email_cc.split(",")
                bcc      = question.email_bcc.split(",")
                user_notification.append( dict(
                    subject     = "%s (%s)" % ( project.description, feedback_pk ),
                    body        = text_msg,
                    from_email  = question.email_from,
                    to          = [email],
                    cc          = cc,
                    bcc         = bcc,
                ))

        data = dict(
            feedback_notification = feedback_notification,
            user_notification     = user_notification,
        )
        result = tasks.post_processing.apply_async( args=[ data ], queue=settings.DEFAULT_QUEUE )


        # post submission redirect URL
        if project.redirect_url:  redirect_url = project.redirect_url
        else:                     redirect_url = urlresolvers.reverse( "forms:thanks", args=[reference] )
        request.session["answer_dict"] = answer_dict

        # paypal
        def get_amount( field ):
            try:   return float( request.POST[ field ] )
            except ValueError: return 0
        amount = sum( [ get_amount(f) for f in request.POST.getlist( "_paypal" ) ] )
        if amount:
            notify_url  = "https://%s" % settings.PAYPAL_REDIRECT_HOSTNAME + urlresolvers.reverse('paypal:paypal-ipn')
            paypal_dict = {
                "business":      settings.PAYPAL_RECEIVER_EMAIL,
                "amount":        "%.2f" % amount,
                "item_name":     "New Creation Church %s" % project.description,
                "invoice":       "%s#%s" % ( project.reference, feedback_pk ),
                "notify_url":    notify_url,
                "return_url":    "https://%s" % settings.PAYPAL_REDIRECT_HOSTNAME + redirect_url,
                "cancel_return": "https://www.newcreation.org.sg",
                "currency_code": "SGD",
            }
            form    = PayPalPaymentsForm( initial=paypal_dict )
            context = { "form": form }
            return render( request, "paypal.html", context )


        return HttpResponseRedirect( redirect_url )



    #--------------------------------------------
    def post( self, request, reference ):
    #--------------------------------------------
        feedback_pk = request.POST.get("feedback_pk")
        if feedback_pk:
            return self.post_processing( request, reference, feedback_pk )
        else:
            return self.pre_processing( request, reference )

