from __future__ import absolute_import
import json

def date_handler(obj):
    try:   return obj.isoformat()
    except AttributeError:
           return obj

def json_dumps(*args, **kwargs):
    kwargs["default"] = kwargs.get("default", date_handler)
    kwargs["indent" ] = kwargs.get("indent",  4)
    return json.dumps( *args, **kwargs )



# Celery
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celeryapp import app as celery_app


