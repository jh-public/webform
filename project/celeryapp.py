from __future__ import absolute_import

import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

from django.conf import settings

app = Celery( 'project' )

# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object( 'django.conf:settings' )
app.autodiscover_tasks( lambda: settings.INSTALLED_APPS )



# from feedback.models import *
# from feedback.views  import serialize


@app.task(bind=True)
def debug_task(self):
    x='Request: {0!r}'.format(self.request)
    print x
    return x
