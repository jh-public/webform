# stgforms.newcreation.org.sg - staging/test server

DEBUG               = True

SOCIAL_AUTH_REDIRECT_IS_HTTPS   = False
SESSION_COOKIE_SECURE           = False

DATABASES = {
    'default': {
        'ENGINE':           'django.db.backends.postgresql_psycopg2',
        'NAME':             'forms_stg',
        'USER':             'stgcode',
        'PASSWORD':         'qwer1234',
        'HOST':             'stg-forms.cjob0gqwzplx.us-west-2.rds.amazonaws.com',
        'PORT':             '5432',
        'ATOMIC_REQUESTS':  True,
    }
}

EMAIL_USE_TLS       = True
EMAIL_HOST          = 'smtp.mandrillapp.com'
EMAIL_PORT          = 587
EMAIL_HOST_USER     = 'noah@newcreation.org.sg'
EMAIL_HOST_PASSWORD = 'O3P0jXMHdGA6aIchTN5j2g'
EMAIL_BACKEND       = 'django.core.mail.backends.smtp.EmailBackend'

PAYPAL_REDIRECT_HOSTNAME = "stgforms.newcreation.org.sg"

BROKER_URL          = 'amqp://broker//'
DEFAULT_QUEUE       = "ncc_django-default"

GA_CODE = ""
