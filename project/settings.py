import os
BASE_DIR        = os.path.dirname(os.path.dirname(__file__))
SITE_ID         = 1
SECRET_KEY      = 'y5dq=0junjdc&u5h**0+@l_j*x)h1#d7=7l%4as%ut74ttunf@'
ALLOWED_HOSTS   = [ "*" ]

INSTALLED_APPS = (
    'feedback.apps.FeedbackConfig',
    'social.apps.django_app.default',
    'django_nvd3',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'debug_toolbar',
    'paypal.standard.ipn',
    'djcelery',
    'oauth2_provider',
    'rest_framework',
    'api',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
)

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

AUTHENTICATION_BACKENDS = (
    'social.backends.open_id.OpenIdAuth',
    'social.backends.google.GoogleOpenId',
    'social.backends.google.GoogleOAuth2',
    'social.backends.google.GoogleOAuth',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.yahoo.YahooOpenId',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF        = 'project.urls'
WSGI_APPLICATION    = 'project.wsgi.application'

GRAPPELLI_ADMIN_TITLE               = "New Creation Church"

DEBUG_TOOLBAR_PATCH_SETTINGS        = False

SOCIAL_AUTH_URL_NAMESPACE           = 'social'
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY       = "437075580143-6h4flb1v1o3sf53qsbg790bj398pmuor.apps.googleusercontent.com"
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET    = "2H0uUXERVRmEdBpvsVosoVJQ"

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details'
)

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE     = 'Asia/Singapore'
USE_I18N      = True
USE_L10N      = False
USE_TZ        = False


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL  = '/static/'
STATIC_ROOT = os.path.join( BASE_DIR, 'project', 'static' )

LOGIN_URL           = '/login/'
LOGIN_REDIRECT_URL  = '/'

DATE_FORMAT             =  'd-b-Y'
TIME_FORMAT             =  'H:i'
DATETIME_FORMAT         =  'd-b-Y H:i'
DATE_INPUT_FORMATS      = ('%d-%b-%Y',       '%Y-%b-%d',       '%Y-%m-%d',       '%d-%m-%Y')
DATETIME_INPUT_FORMATS  = ('%d-%b-%Y %H:%i', '%Y-%b-%d %H:%i', '%Y-%m-%d %H:%i', '%d-%m-%Y %H:%i')



# django-paypal
PAYPAL_RECEIVER_EMAIL = "events-facilitator@newcreation.org.sg"
PAYPAL_TEST = True


# CELERY SETTINGS
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERYD_CONCURRENCY = 1
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
CELERY_IMPORTS = ('feedback.tasks', )


# DRF
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'oauth2_provider.ext.rest_framework.OAuth2Authentication',
        'rest_framework.authentication.SessionAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
        'rest_framework.permissions.DjangoModelPermissions',
    ],
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 10,
}

# DOT
OAUTH2_PROVIDER = {
    'SCOPES': {
        'read':             'Read scope',
        'write':            'Write scope',
        'forms':            'Forms scope',
    },
    'DEFAULT_SCOPES': [],
    'READ_SCOPE'    : 'read',
    'WRITE_SCOPE'   : 'write',
}



##### local settings

SOCIAL_AUTH_REDIRECT_IS_HTTPS   = False
SESSION_COOKIE_SECURE           = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': '_postgres_',
        'HOST': 'db',
        'PORT': '',
    },
}


# email
DEFAULT_FROM_EMAIL  = 'pandalover8350@gmail.com'
EMAIL_USE_TLS       = True
EMAIL_HOST          = 'smtp.gmail.com'
EMAIL_PORT          = 587
EMAIL_HOST_USER     = 'pandalover8350@gmail.com'
EMAIL_HOST_PASSWORD = 'Abc23456'
EMAIL_BACKEND       = 'django.core.mail.backends.console.EmailBackend'
EMAIL_BACKEND       = 'django.core.mail.backends.smtp.EmailBackend'


# forms
PAYPAL_REDIRECT_HOSTNAME = None


# celery
BROKER_URL     = 'amqp://broker//'
DEFAULT_QUEUE  = os.environ.get( 'QUEUE', 'webform-default' )


# Google Analytics
GA_CODE = ""


DEBUG = False
DEBUG = True

try:    from local_settings import *
except: pass

