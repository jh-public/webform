'use strict';

angular.module('forms.FeedbackSummary', ['djangoData', 'ncc.directives'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller( "feedbackSummaryController", [
    "$scope",
    "djangoData",
    function(
        $scope,
        djangoData
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.django[k] = v;
        });

        this.dateFrom    = $scope.django.dateFrom;
        this.dateTo      = $scope.django.dateTo;

        this.refresh = function() {
            if (!this.dateFrom) return;
            if (!this.dateTo  ) return;
            location.href = location.origin + location.pathname + "?from=" + this.dateFrom + "&to=" + this.dateTo;
        };

    }
])

