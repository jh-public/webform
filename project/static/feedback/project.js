'use strict';

angular.module('forms', ['djangoData'])


.controller("feedback.questionnaireEditCtrl",[
    "$scope",
    "$timeout",
    "djangoData",
    function(
        $scope,
        $timeout,
        djangoData
    ){
        $scope.dependsOn = [];


        function trim(str) {
            return str.replace(/^\s+|\s+$/gm, '');
        }

        function getOptionsList(input) {
            input = input.split("\n");
            var output = []
            angular.forEach(input, function(value, idx) {
                value = trim( value.split("\r")[0] );
                if (value) output.push(value);
            });
            return output;
        };


        // djangodata -> $scope
        angular.forEach(djangoData, function(v, k) {
            $scope[k] = v;
        });


        // Django form fields
        $("input").addClass("form-control");
        $("input[type=checkbox]").removeClass("form-control");
        $("select").addClass("form-control");
        $("textarea").addClass("form-control").attr("rows", 4);
        $("input[name$=question_no]").attr("required", "required").attr("min", "1");
        $("textarea[name$=question]").attr("required", "required");
        $("#id_questions_set-0-options").attr("placeholder", "Choices for multiple choice/answer question.  One choice per line.")
        $("select[name$=answer_type]").change(answerTypeChange);
        $("select[name$=answer_type]").each( function() { answerTypeChange(null, this); } );


        // Management-form
        $scope.qcount = parseInt( $("#id_questions_set-TOTAL_FORMS").val() );
        $scope.newQs = [];
        $scope.addQuestion = function(e) {
            $scope.newQs.push( $scope.qcount );
            $("#id_questions_set-TOTAL_FORMS").val(++$scope.qcount);
            $timeout( function(){
                var elems = $("select[name$=answer_type]");
                elems[elems.length - 1].focus();
            }, 100);
        };

        // on-submit validation check
        $scope.submit = function(e) {

            function getQInfoInput(seq, name) {
                return $("#id_questions_set-"+seq+"-"+name);
            }

            var qobjs       = {};
            var questions   = $("input[name$=question_no]");

            questions.each( function() {
                var qno            = this.value;
                qobjs[qno]         = this;
                var seq            = $(this).attr("data-seq");
                var depends_on     = getQInfoInput(seq, "depends_on");
                var depends_value  = getQInfoInput(seq, "depends_value");
                if (depends_on.length)    depends_on[0].setCustomValidity('');
                if (depends_value.length) depends_value[0].setCustomValidity('');

                var parent_qno  = depends_on.val();
                if (!parent_qno) return;

                var parentq = qobjs[parent_qno];
                if (!parentq) {
                    depends_on[0].setCustomValidity('Invalid parent question no.');
                    return false;
                }

                var parent_seq  = parentq.getAttribute("data-seq");
                var parent_type = getQInfoInput(parent_seq, "answer_type").val();
                var valid_types = ["radio", "checkbox", "multicheckbox"];
                if ( valid_types.indexOf(parent_type) == -1 ) {
                    depends_on[0].setCustomValidity('Invalid type for parent question no.  Parent must be multiple choice/answer, yes/no.');
                    return false;
                }

                var depends_value  = getQInfoInput(seq, "depends_value");
                var value          = trim( depends_value.val() );
                depends_value.val(value);
                var parent_options = getOptionsList( getQInfoInput(parent_seq, "options").val() );
                if ( parent_options.indexOf(value) == -1 ) {
                    depends_value[0].setCustomValidity('Invalid parent value.');
                    return false;
                }
            });

            var form = $("#form1")[0];
            form.checkValidity() && form.submit();
        }

        // onchange: conditional question number
        $scope.changeDepends = function(no) {
            var depends = $( "#id_questions_set-" + no + "-depends_on option:selected" ).text();
            var elem = $( "#id_questions_set-" + no + "-depends_value" );
            if (depends)  {
                elem.removeAttr("disabled");
                elem.attr("required", "required");
                elem.val("");
            } else {
                elem.attr("disabled", "disabled");
                elem.removeAttr("required");
                elem.val("");
            }
        }

        // questions info
        var qnos     = [];
        var qoptions = {};
        angular.forEach( $scope.questions_info, function(v,k) {
            qnos.push(v.question_no);
            qoptions[v.question_no] = v.options;
        });

        // conditional question numbers
        $scope.conditionalQnos = [];
        angular.forEach( $scope.questions_info, function(v,k) {
            $scope.conditionalQnos[k] = qnos;
        });
        $scope.getConditionalQnos = function(qno) {
            $scope.conditionalQnos[qno] = [];
            $("input[name$=question_no]").each( function() {
                var elem    = $(this);
                var dataseq = parseInt( elem.attr("data-seq") );
                var anstype = $("#id_questions_set-" + dataseq + "-answer_type").val();
                if ( ["radio", "checkbox", "multicheckbox"].indexOf(anstype) != -1 && dataseq < qno ) {
                    $scope.conditionalQnos[qno].push(elem.val());
                }
            });

        };

        // conditional question values
        $scope.conditionalQVals = [];
        angular.forEach( $scope.questions_info, function(v,k) {
            var options = qoptions[v.depends_on];
            if (options) $scope.conditionalQVals[k] = getOptionsList(options);
        });
        $scope.getConditionalQVals = function(qno) {
            var dependsQno = $("#id_questions_set-" + qno + "-depends_on").val()
            var dataseq;
            $("input[name$=question_no]").each( function() {
                var elem = $(this);
                if (elem.val() == dependsQno) {
                    dataseq = elem.attr("data-seq");
                    return false;
                }
            });
            var val = $("#id_questions_set-" + dataseq + "-options").val();
            $scope.conditionalQVals[qno] = getOptionsList(val);
        };


        // increase/decrease question no.
        function changeQNo(no, offset) {
            var qelem       = $("input[name$=set-"+no+"-question_no]");
            var qval        = qelem.val();
            if (!qval) return;

            var qval_1      = parseInt(qval) + offset;
            var qelem_1     = false;
            $("input[name$=question_no]").each( function() {
                if (this.value == qval_1) {
                    qelem_1 = $(this);
                    return false;
                }
            });
            if (!qelem_1) return;
            qelem.val(qval_1);
            qelem_1.val(qval);
            $("*[name$=depends_on]").each( function() {
                if      (this.value == qval)   this.value = qval_1;
                else if (this.value == qval_1) this.value = qval;
            });
        }
        $scope.increase     = function(no) {
            return changeQNo( no, 1 );
        };
        $scope.decrease     = function(no) {
            return changeQNo( no, -1 );
        };

    }

])


function answerTypeChange(e, elem) {
    var elem  = (e)? e.currentTarget : elem;
    var eid   = elem.id.split("-");
    eid[2]    = "options";
    eid       = eid.join("-");
    var elem2 = $("#"+eid);

    if ( elem.value == "checkbox" ) {
        elem2.removeAttr("disabled");
        elem2.removeAttr("required");
        elem2.attr("readonly", "readonly");
        elem2.text("Yes\nNo");
    } else if ( elem.value == "radio" || elem.value == "multicheckbox" ) {
        elem2.removeAttr("disabled");
        elem2.removeAttr("readonly");
        elem2.attr("required", "required");
    } else {
        elem2.attr("disabled", "disabled");
        elem2.removeAttr("required");
        elem2.text("");
    }
}


