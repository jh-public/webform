'use strict';


function trim(str) {
    return str.replace(/^\s+|\s+$/gm, '');
}


angular.module('forms', ['djangoData', 'ncc.directives'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller("feedback.questionEditCtrl",[
    "$scope",
    "djangoData",
    function(
        $scope,
        djangoData
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.django[k] = v;
        });

        $scope.django.question.options         = $scope.django.question.options         || "";
        $scope.django.question.options_columns = $scope.django.question.options_columns || 12;

        // question
        this.question = $scope.django.question;

        // widths
        this.col_widths = $scope.django.question.col_widths;
        if (this.col_widths) {
            this.widths  = [];
            angular.forEach( this.col_widths.split(","),  function(v) { this.widths.push(parseInt(v));  }.bind(this) );
        } else {
            this.col_widths  = "12,12,12,12";
            this.widths      = [12,12,12,12];
        }

        // minmax
        if ( $scope.django.question.answer_type == "number" ||
             $scope.django.question.answer_type == "paypal" ||
             $scope.django.question.answer_type == "text" ) {
            this.showMinMax = true;
        } else {
            this.showMinMax = false;
        }

        // offsets
        this.col_offsets = $scope.django.question.col_offsets;
        if (this.col_offsets) {
            this.offsets = [];
            angular.forEach( this.col_offsets.split(","), function(v) { this.offsets.push(parseInt(v)); }.bind(this) );
        } else {
            this.col_offsets = "0,0,0,0";
            this.offsets     = [0,0,0,0];
        }

        // widthChanged()
        this.widthChanged = function(idx) {
            debugger
            var w = parseInt( this.widths[idx] );
            angular.forEach( this.widths, function(v, k) {
                if ( k > idx && v > w )  this.widths[k] = w;
            }.bind(this));
            this.col_widths=this.widths.join(',');
        }

        // choiceCategoryChanged()
        this.choiceCategoryChanged = function() {
            this.optionsRequired = this.optionType && this.choiceCategory == "";
        }.bind(this);

        // stripTags()
        this.stripTags = function( input ) {
            var txt = input.replace( /<([^>]+>)/g, "");
            if (txt.length > 100) txt = txt.substr(0,100) + "...";
            return txt;
        };
    }
])



.directive( "gnCheckbox", [function() {
    return {
        restrict:    "E",
        templateUrl: "checkbox.html",
        scope:       {
            htmlName:   "=",
            label:      "=",
            checked:    "=",
        },
    };
}])



.directive( "gnDependsValue", [function() {
    return {
        restrict:    "E",
        templateUrl: "depends_value.html",
        scope:       {
            htmlName:   "=",
            label:      "=",
            ctrl:       "=",
            questions:  "=",
            value:      "=",
        },
        link: function(scope, element, attrs) {
            scope.ctrl.depends_value = scope.value;
            scope.questionsObj = {};
            angular.forEach(scope.questions, function(v,k,o) {
                scope.questionsObj[v.pk] = v;
            });
        },
    };
}])



.directive( "gnAnswerType", [function() {
    return {
        restrict:    "E",
        templateUrl: "answer_type.html",
        scope:       {
            htmlName:   "=",
            label:      "=",
            ctrl:       "=",
            choices:    "=",
            value:      "=",
        },
        link: function(scope, element, attrs) {
            var optionTypes         = [ "radio", "multicheckbox", "checkbox" ];
            var optionColumnsTypes  = [ "radio", "multicheckbox" ];
            var options_elem        = $("*[name=options]");
            scope.ctrl.answer_type        = scope.value;
            scope.ctrl.optionType         = optionTypes.indexOf(scope.value) != -1;
            scope.ctrl.optionsRequired    = scope.ctrl.optionType && scope.ctrl.choiceCategory == "";
            scope.ctrl.showOptionsColumns = optionColumnsTypes.indexOf(scope.value) != -1;

            if (scope.value == "checkbox") {
                options_elem.attr("readonly", "readonly");
            }
            scope.answerTypeChanged = function() {
                scope.ctrl.optionType         = optionTypes.indexOf(scope.ctrl.answer_type)        != -1;
                scope.ctrl.optionsRequired    = scope.ctrl.optionType && scope.ctrl.choiceCategory == "";
                scope.ctrl.showOptionsColumns = optionColumnsTypes.indexOf(scope.ctrl.answer_type) != -1;

                if (scope.ctrl.optionsRequired) {
                    options_elem.removeAttr("readonly");
                }

                if (scope.ctrl.answer_type == "checkbox") {
                    scope.ctrl.options        = "Yes\nNo";
                    scope.ctrl.choiceCategory = "";
                    options_elem.attr("readonly", "readonly");
                }

                if ( scope.ctrl.answer_type == "number" ||
                     scope.ctrl.answer_type == "paypal" ||
                     scope.ctrl.answer_type == "text" ) {
                    scope.ctrl.showMinMax = true;
                } else {
                    scope.ctrl.showMinMax = false;
                }
            }
        },
    };
}])



.filter( "splitOptions", function() {
    return function(input) {
        var output = []
        if (!input) return output;
        input = input.split("\n");
        angular.forEach(input, function(value, idx) {
            value = trim( value.split("\r")[0] );
            if (value) output.push(value);
        });
        return output;
    };
})

