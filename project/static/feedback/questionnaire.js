'use strict';

angular.module('ncc', ['ngAnimate', 'ncc.directives', 'djangoData', 'validators'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller("feedback.questionnaireCtrl",[
    "$scope",
    'djangoData',
    function(
        $scope,
        djangoData
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope[k] = v;
        });

        $scope.debug                 = !true;
        $scope.answers               = {};
        $scope.answersEmailConfirm   = {};
        $scope.dependants            = {};
        $scope.errormsg              = {};
        $scope.prev_pages            = [];
        $scope.pageinfo              = {};
        $scope.pageinfo.current_page = 1;
        $scope.pageinfo.pct          = 0.1;
        $scope.feedback_pk           = document.form1.feedback_pk.value;
        $scope.submitted             = !($scope.feedback_pk == "");

        // questions dicts
        var questions = {};
        angular.forEach( $scope.questions, function(value, key) {
            value.widths  = value.fields.col_widths.split(",");
            value.offsets = value.fields.col_offsets.split(",");
            questions[value.fields.question_no] = value;
        });

        // initial values
        angular.forEach( $scope.initial, function(value, key) {
            if (key.substr(0,1) == "Q") key = key.substr(1,99);
                 if ( questions[key] ) questions[key].initial = value;
        });

        // get pages
        function getPageQuestions() {
            var pages = [];
            var qs    = []
            for (var i=0, len=$scope.questions.length; i<len; i++) {
                var q   = $scope.questions[i];
                var qno = q.fields.question_no;
                $scope.dependants[qno] = [];

                if (q.fields.answer_type == "multicheckbox") {
                    $scope.answers[qno] = [];
                }
                else if ( ["text", "hidden", "readonly"].indexOf(q.fields.answer_type) != -1 ) {
                    if ( questions[qno].initial )  $scope.answers[qno] = questions[qno].initial;
                }

                // conditional questions
                var depends_on = q.fields.depends_on;
                if (depends_on) {
                    q.hide = true;
                    var parentq = questions[depends_on];
                    if (parentq.fields.answer_type == "checkbox" && q.fields.depends_value == "No") q.hide = false;
                    $scope.dependants[depends_on] = $scope.dependants[depends_on] || [];
                    $scope.dependants[depends_on].push(q);
                }

                qs.push(q);
                if (q.fields.page_break) {
                    pages.push(qs);
                    qs = [];
                }
            }
            if (qs.length) pages.push(qs);
            return pages;
        }
        $scope.pages = getPageQuestions();

        // questions page#
        $scope.questionsPageNo = {}
        angular.forEach( $scope.pages, function( questions, pageNo ) {
            var qpn = $scope.questionsPageNo;
            angular.forEach( questions, function( question, i ) {
                qpn[ question.fields.question_no ] = pageNo;
            });
        });

}]) // controller



.filter('nonBlanks', [ function() {
    return function(input) {

        function trim(str) {
            return str.replace(/^\s+|\s+$/gm, '');
        }

        input = input.split("\n");
        var output = []
        angular.forEach(input, function(value, idx) {
            value = trim( value.split("\r")[0] );
            if (value) output.push(value);
        });
        return output;
    };
}])



.filter('unsafe', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}])


.directive( "gnConsentChk", [ 'submit_form', function( submit_form ) {
    return {
        restrict: "A",
        link: function($scope, element, attrs) {

            element.on("click", function(e) {
                var form = $("#form1")[0];
                if ( form.checkValidity() ) {
                    submit_form($scope);
                    return;
                }
                $("#btn-submit-hidden").click();
            });
        },
    };
}])



.directive( "gnGoPrev", [function() {
    return {
        restrict:   "A",
        link: function($scope, element, attrs) {
            element.on("click", function(e) {
                var currPage = $scope.prev_pages.pop();
                $('#page-tabs li:eq(' + currPage + ') a').tab('show');
                $scope.pageinfo.current_page = currPage + 1;
                $scope.pageinfo.pct          = Math.min(100, 100 * currPage / $scope.pages.length) || 0.1;
                $scope.$apply();
                document.body.scrollTop = document.documentElement.scrollTop = 0;
            });
        },
    };
}])



.factory( "resetDependants", [ function() {
    return function resetDependants( $scope, qno ) {
        var dependants  = $scope.dependants[qno];
        for (var i=0, len=dependants.length; i<len; i++) {
            var q       = dependants[i];
            var depQno  = q.fields.question_no;
            q.hide=true;
            $scope.answers[depQno] = undefined;
            resetDependants( $scope, depQno );
        }
    }
}])



.directive( "gnAnswerChanged", [ "resetDependants", function( resetDependants ) {
    return {
        restrict:   "A",
        link: function($scope, element, attrs) {

            function answerChanged(e) {
                var qno         = attrs.qno;
                var answer      = $scope.answers[qno];
                var dependants  = $scope.dependants[qno];
                if (typeof(answer) == "boolean") answer = (answer)? "Yes" : "No";
                for (var i=0, len=$scope.dependants[qno].length; i<len; i++) {
                    var q       = $scope.dependants[qno][i];
                    var depQno  = q.fields.question_no;
                    if (q.fields.depends_value == answer)   {
                        q.hide=false;
                    } else {
                        q.hide=true;
                        $scope.answers[depQno] = undefined;
                        resetDependants( $scope, depQno );
                    }
                }
                $scope.$apply();
            }

            element.on("click",  answerChanged );
            element.on("change", answerChanged );
        },
    };
}])



.directive( "gnAnswerChangedMultiCheckbox", [ "resetDependants", function( resetDependants ) {
    return {
        restrict:   "A",
        link: function($scope, element, attrs) {

            function answerChanged(e) {
                var qno     = attrs.qno;
                var multis  = $("input[name=Q"+qno+"]");
                var answers = [];
                multis.each( function(idx, elem) {
                    if (elem.checked) answers.push(elem.value);
                });

                for (var i=0, len=$scope.dependants[qno].length; i<len; i++) {
                    var q       = $scope.dependants[qno][i];
                    var depQno  = q.fields.question_no;
                    if (answers.indexOf(q.fields.depends_value) == -1 ) {
                        q.hide=true;
                        $scope.answers[depQno] = undefined;
                        resetDependants( $scope, depQno );
                    } else {
                        q.hide=false;
                    }
                }
                $scope.$apply();
            }

            element.on("click",  answerChanged );
            element.on("change", answerChanged );
        },
    };
}])



.directive( "gnAnswerChangedNumber", [function() {
    return {
        restrict:   "A",
        link: function($scope, element, attrs) {
            element.on("change", function(e) {
                var qno         = attrs.qno;
                var answer      = $scope.answers[qno];
                answer = parseFloat( answer );
                if ( !isNaN( answer ) )  $scope.answers[qno] = Math.floor( answer * 100 ) * 0.01;
                $scope.$apply();
            });
        },
    };
}])



.factory( "required_field_validation", [ function() {
    return function (qobj, elems) {
        if (!qobj.fields.required) return true;

        var pageValid = true;
        var fldValid, msg;
        switch (qobj.fields.answer_type) {

            case "radio":
            case "multicheckbox":

                if (qobj.fields.options_columns == -1) { // dropdown
                    fldValid = elems[0].value != "" && elems[0].value != "? undefined:undefined ?"
                    if (!fldValid) {
                        msg = "Please select an answer for this question.";
                    }
                    break;
                }

                fldValid = false;
                elems.each( function(idx, elem) {
                    fldValid = fldValid || elem.checked;
                });
                if (!fldValid) {
                    msg = "Please select an answer for this question.";
                }
                break;

            case "checkbox":
                fldValid = elems[0].checked;
                if (!fldValid) {
                    msg = "This checkbox must be selected.";
                }
                break;

            default:
                fldValid = elems.val().match(/\w+/);
                msg      = "Please provide an answer for this question.";

        } //switch

        if (!fldValid) {
            elems[0].setCustomValidity(msg);
            qobj.error = msg;
            pageValid  = false;
        }

        return pageValid;
    }
}])



.factory( "number_field_validation", [ function() {
    return function (qobj, elems) {
        var elem = elems[0];
        var val  = parseFloat( elem.value );
        var min  = parseFloat( qobj.fields.minimum );
        var max  = parseFloat( qobj.fields.maximum );
        if ( val < min ) {
            qobj.error = "Minimum value is " + min;
            return false;
        }
        if ( val > max ) {
            qobj.error = "Maximum value is " + max;
            return false;
        }
        return true;
    }
}])



.factory( "text_field_validation", [ function() {
    return function (qobj, elems) {
        var elem = elems[0];
        var val  = elem.value.trim().length;
        var min  = parseInt( qobj.fields.minimum );
        var max  = parseInt( qobj.fields.maximum );
        if ( min && val < min ) {
            qobj.error = "Minimum length is " + min;
            return false;
        }
        if ( max && val > max ) {
            qobj.error = "Maximum length is " + max;
            return false;
        }
        return true;
    }
}])



.factory( "email_field_validation", [ "emailValidator", function( emailValidator ) {
    return function ( qobj, elems, scope ) {
        var elem = elems[0];
        if ( !emailValidator(elem) ) {
            qobj.error = "Invalid email address";
            return false;
        }
        if (qobj.fields.email_input_again) {
            var qno    = qobj.fields.question_no;
            var email1 = elem.value;
            var email2 = scope.answersEmailConfirm[qno] || "";
            if (email1 != email2) {
                qobj.error = "Email addresses do not match";
                return false;
            }
        }
        return true;
    }
}])



.factory( "validate_page", [
     '$q'
    ,'required_field_validation'
    ,'number_field_validation'
    ,'text_field_validation'
    ,'email_field_validation'
    ,'nric_fin_validator'
    ,'unique_validator'
//     ,'custom_validation',
    ,function(
         $q
        ,required_field_validation
        ,number_field_validation
        ,text_field_validation
        ,email_field_validation
        ,nric_fin_validator
        ,unique_validator
//         custom_validation
    ) {
        return function(page, $scope) {
            $scope.errormsg.error      = "";
            var validate_page_deferred = $q.defer();
            var djangoValidatorNames   = $scope.validators;
            var valid                  = true;
            var promises               = [];

            // validate page questions
            for( var j=0,lenj=page.length; j<lenj; j++) {
                var qobj       = page[j];
                qobj.error     = "";
                var elems      = $("*[name=Q" + qobj.fields.question_no + "]");
                if (!elems.length)  continue;
                elems[0].setCustomValidity("");

                // required field validation
                valid = valid && required_field_validation(qobj, elems);
                if (!valid) continue;

                // text field validation
                if ( qobj.fields.answer_type == "text" ) {
                    valid = valid && text_field_validation(qobj, elems);
                    if (!valid) continue;
                }

                // number field validation
                if ( qobj.fields.answer_type == "number" || qobj.fields.answer_type == "paypal" ) {
                    valid = valid && number_field_validation(qobj, elems);
                    if (!valid) continue;
                }

                // email field validation
                if ( qobj.fields.answer_type == "email" ) {
                    valid = valid && email_field_validation( qobj, elems, $scope );
                    if (!valid) continue;
                }

                // id & nric field validation
                if (
                    ( qobj.fields.answer_type == "id" && $scope.answers[ qobj.fields.question_no+"_idType" ] == "nric" ) ||
                    qobj.fields.answer_type == "nric"

                ) {
                    promises.push( nric_fin_validator( elems[0], qobj ) );
                }

                // unique validation
                if ( qobj.fields.unique ) {
                    promises.push( unique_validator( elems[0], qobj ) );
                }

                // custom validation
                // promises.push( $q.when( custom_validation( qobj, elems, djangoValidatorNames ) ));
            }

            if (!valid)                 validate_page_deferred.reject();
            else if (promises.length)   $q.all( promises ).then( validate_page_deferred.resolve, validate_page_deferred.reject );
            else                        validate_page_deferred.resolve();

            return validate_page_deferred.promise;
        };
    }
])



.factory( "submit_form", [ '$location', '$http', "frsc", function( $location, $http, frsc ) {
    return function($scope) {
        if ($scope.submitted) {
            $scope.errormsg.error = "Form has already been submitted.";
            return;
        }
        $scope.submitted = true;
        var csrf = frsc.split("").reverse().join("")
        $scope.csrfmiddlewaretoken = csrf;
        document.form1.csrfmiddlewaretoken.value = csrf;

        function encode( elem ) {
            return encodeURIComponent( elem.name ) + "=" + encodeURIComponent( elem.value );
        }

        // form data
        var str = [];
        angular.forEach( document.form1.elements, function( val, i ) {
            var type = val.type;
            if (!val.name) return;

            if ( type == "radio" || type == "checkbox" ) {
                if ( val.checked ) {
                    str.push( encode( val ) );
                }
                return;
            }
            str.push( encode( val ) );
        });
        var data = str.join("&");

        // post
        $http({
            method: 'POST',
            url: $location.absUrl(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest: function() {
                return data;
            },
            data: {}
        }).then(

            function(data)  {
                data = data.data;
                if (!data.feedback) {
                    if (data.error) {
                        $scope.errormsg.error = data.message;
                        console.log(data);
                        var qno = data.question;
                        if (qno) {
                            $scope.answers[qno]=undefined;
                            var jelem = $("input[name=Q"+qno+"][value='"+data.answer+"']");
                            jelem.prop("disabled", true);
                            jelem.siblings( "span" ).addClass( "disabled" );
                            var qPageNo  = $scope.questionsPageNo[ qno ];
                            do {
                                var currPage = $scope.prev_pages.pop();
                            } while ( qPageNo < currPage )
                            $('#page-tabs li:eq(' + currPage + ') a').tab('show');
                            $scope.pageinfo.current_page = currPage + 1;
                            $scope.pageinfo.pct = Math.min(100, 100 * currPage / $scope.pages.length) || 0.1;
                            $scope.$apply();
                            document.body.scrollTop = document.documentElement.scrollTop = 0;
                        }
                    } else {
                        $("#debug").html( data );
                        debugger;
                    }
                } else {
                    document.form1.feedback_pk.value = data.feedback;
                    $("#form1")[0].submit();
                }
            },

            function(error) {
                $("#debug").html( error.data );
                console.log(error)
            }
        );

    };
}])



.directive( "gnGoNext", [ 'validate_page', 'submit_form', function( validate_page, submit_form ) {
    return {
        restrict:   "A",
        link: function($scope, element, attrs) {

            element.on("click", function(e) {
                var currPage = parseInt(attrs.currentPage);
                var page     = $scope.pages[currPage];
                var lastq    = page[page.length-1].fields.question_no;

                function next_page() {

                    // find next non-empty page
                    outer:
                    for (var nextpage, i=currPage + 1, len_i=$scope.pages.length; i<len_i; i++) {
                        var questions = $scope.pages[i];
                        for (var j=0, len_j=questions.length; j<len_j; j++) {
                            if ( !questions[j].hide ) {
                                nextpage = i;
                                break outer;
                            }
                        }
                    }

                    // page numbers & progress
                    $scope.prev_pages.push(currPage);
                    currPage            = nextpage || $scope.pages.length;
                    $scope.pageinfo.pct = Math.min(100, 100 * currPage / $scope.pages.length);
                    if ( !$scope.project.consent_page && currPage == $scope.pages.length )  submit_form($scope);
                    $('#page-tabs li:eq(' + currPage + ') a').tab('show');
                    $scope.pageinfo.current_page = currPage + 1;
                    // $scope.$apply();
                    document.body.scrollTop = document.documentElement.scrollTop = 0;
                }

                function error_on_page() {
                    $scope.errormsg.error = "Error(s) found on page."
//                     if (Modernizr.input.required) $("#btn-submit-hidden").click();
//                     $scope.$apply();
                    return;
                }

                validate_page( page, $scope ).then( next_page, error_on_page );

            }); // onclick

        },  // link
    };
}])
