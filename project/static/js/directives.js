'use strict';

angular.module( 'ncc.directives', [] )



.directive( "gnDeleteConfirmation", [function() {
    return {
        restrict:    "E",
        templateUrl: "/static/partials/delete-confirmation.html",
        scope:       {
            csrftoken:  "@",
            title:      "@",
            text:       "@",
            action:     "@",
            name:       "@",
        },
    };
}])


.directive( "gnDatePicker", [function() {
    return {
        restrict: "A",
        link: function( $scope, element, attrs ) {
            if (
                navigator.userAgent.indexOf("Android") != -1 ||
                navigator.userAgent.indexOf("iPad")    != -1 ||
                navigator.userAgent.indexOf("iPhone")  != -1
            ) {
                $(element).attr("type", "date");
            } else {
                $(element).attr("type", "text");
                $(element).attr("data-provide", "datepicker");
                var format = attrs.dateFormat || "d-M-yyyy";
                $(element).attr("data-date-format", format);
            }
        },
    };
}])


.directive( "gnMonthPicker", [function() {
    return {
        restrict: "A",
        link: function( $scope, element, attrs ) {
            if (
                navigator.userAgent.indexOf("Android") != -1 ||
                navigator.userAgent.indexOf("iPad")    != -1 ||
                navigator.userAgent.indexOf("iPhone")  != -1 ||
                navigator.userAgent.indexOf("Chrome")  != -1
            ) {
                $(element).attr("type", "month");
            } else {
                $(element).attr("type", "text");
                $(element).attr("data-provide", "datepicker");
                var format = attrs.dateFormat || "yyyy-mm";
                $(element).attr("data-date-format", format);
                $(element).attr("data-date-min-view-mode", "1");
            }
        },
    };
}])


.directive( "gnErrorAlert", [function() {
    return {
        restrict:    "E",
        templateUrl: "/static/partials/gnErrorAlert.html",
        scope:       {
            message: "=",
        }
    }
}])

