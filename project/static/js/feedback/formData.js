'use strict';

angular.module('forms', ['djangoData'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller( "formsDataController", [
    "$scope",
    "djangoData",
    function(
        $scope,
        djangoData
    ){
        // djangodata -> $scope
        $scope.django = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.django[k] = v;
        });

        $scope.toggleShowAll = function() {
            $scope.showAll = !$scope.showAll;
        }

    }
])

