'use strict';

angular.module('forms', ['djangoData'])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.controller("Feedback.projectsController",[
    "$scope",
    "djangoData",
    function(
        $scope,
        djangoData
    ){
        // djangodata -> $scope
        $scope.data = {};
        angular.forEach(djangoData, function(v, k) {
            $scope.data[k] = v;
        });

        $scope.status = "Active";
        $scope.origin = window.location.origin;
        $scope.group  = parseInt( sessionStorage.getItem( "forms-groupFilter" ) ) || "";

        $scope.groupFilter = function( project, index, projects ){
            if ( $scope.group == "" || $scope.group == null )  return true;
            var groups = project.perms_read.split( "," );
            return groups.indexOf( String( $scope.group ) ) != -1;
        };

        $scope.groupFilterChanged = function(){
            var value = $scope.group || "";
            sessionStorage.setItem( "forms-groupFilter", value );
        };
    }

])

