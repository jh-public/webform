"use strict";

// keys must be consistent with models.py
angular.module('validators', [])

.config(['$compileProvider', function ($compileProvider) {
  $compileProvider.debugInfoEnabled(false);
}])



.factory( "nric_fin_validator", [ "$q", "validatorErrorMessages", function( $q, validatorErrorMessages ) {
    return function(elem, qobj) {

        function _validator(e, t) {
            t == null && (t = "NRIC");
            if (e.length != 9) return false;

            var n=new Array(9);
            for (var i=0; i<9; i++) n[i] = e.charAt(i);
                n[1] *= 2,
                n[2] *= 7,
                n[3] *= 6,
                n[4] *= 5,
                n[5] *= 4,
                n[6] *= 3,
                n[7] *= 2;

            var r = 0;
            for(var i=1; i<8; i++) r += parseInt(n[i]);

                var s = n[0] == "T" || n[0] == "G" ? 4: 0,
                o = (s+r)%11,
                u = Array("J","Z","I","H","G","F","E","D","C","B","A"),
                a = Array("X","W","U","T","R","Q","P","N","M","L","K"),
                f;

            if (t=="NRIC") {
                if (n[0] == "S" || n[0] == "T") f=u[o];
            } else {
                if (t != "FIN") return false;
                if (n[0] == "F" || n[0] == "G") f=a[o];
            }
            return n[8] == f;
        }

        var deferred = $q.defer();
        var valid    = false;
        var idno     = $(elem).val().trim().toUpperCase();
        if (!idno) {
            valid = true;
        } else {
            var char1 = idno.charAt(0);
            if      ("ST".indexOf(char1) != -1) valid = _validator(idno, "NRIC");
            else if ("FG".indexOf(char1) != -1) valid = _validator(idno, "FIN" );
        }

        if (valid)  {
            deferred.resolve();
        } else {
            var errmsg = validatorErrorMessages["NRIC/FIN"];
            qobj.error = errmsg;
            deferred.reject( errmsg );
        }
        return deferred.promise;
    }
}])



.factory( "emailValidator", [ function() {
    return function(elem) {
        var valid    = false;
        var value    = (angular.isString(elem))? elem : $(elem).val();
        if (!value) {
            valid = true;
        } else {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            valid = re.test(value);
        }
        return valid;
    }
}])



.factory( "email_validator", [ "$q", "emailValidator", "validatorErrorMessages", function( $q, emailValidator, validatorErrorMessages ) {
    return function(elem) {
        var deferred = $q.defer();
        if ( emailValidator(elem) ) deferred.resolve();
        else                        deferred.reject( validatorErrorMessages.Email );
        return deferred.promise;
    }
}])



.factory( "unique_validator", [ "$q", "$http", "validatorErrorMessages", function( $q, $http, validatorErrorMessages ) {
    return function( elem, qobj ) {
        var deferred = $q.defer();
        if (elem.type=="radio" || elem.type=="checkbox") {
            if (!elem.checked) {
                deferred.resolve();
                return deferred.promise;
            }
        }
        var project  = $("input[name=project]").val();
        var qno      = $(elem).attr("data-qno");
        var value    = $(elem).val();
        if (!value) {
            deferred.resolve();
        } else {
            $http.post(
                "/forms/unique/",
                {
                    project: project,
                    qno:     qno,
                    value:   value
                }
            ).then( function(data) {
                if ( data.data == "0" ) {
                    deferred.resolve();
                } else {
                    var errmsg = qobj.fields.msg_err_unique || validatorErrorMessages.Unique;
                    qobj.error = errmsg;
                    deferred.reject( errmsg );
                }
            });
        }
        return deferred.promise;
    }
}])



.factory( 'validators', [
    "nric_fin_validator",
    "email_validator",
    "unique_validator",
    function(
        nric_fin_validator,
        email_validator,
        unique_validator
    ){
    return {
        "NRIC/FIN": nric_fin_validator,
        "Email":    email_validator,
        "Unique":   unique_validator
    }
}])



.value( 'validatorErrorMessages', {
    "Unique":   "Answer has already been recorded in the database.",
    "Email":    "Invalid email address.",
    "NRIC/FIN": "Invalid NRIC/FIN.",
})
