from django.conf.urls.static    import static
from django.conf.urls           import include, url
from django.contrib.auth        import logout
from django.contrib             import admin
from django.http                import HttpResponseRedirect
from django.shortcuts           import redirect
from django.views.generic       import TemplateView

import django.contrib.auth.views as auth_views
import feedback.urls
import settings


def signout(request):
    logout(request)
    return redirect( "/%s/" % request.GET.get( "app_name", "forms" ))


urlpatterns = [
    url( r'^logout/$',          signout),
    url( r'^password_change/$', auth_views.password_change ),
    url( r'^password_reset/$',  auth_views.password_reset, { 'extra_email_context': { 'protocol': 'https' } }, name='custom_password_reset' ),
    url( '',                    include( 'social.apps.django_app.urls', namespace='social')),
    url( '',                    include( 'django.contrib.auth.urls')),

    url( r'^$',                 lambda x:HttpResponseRedirect( '/forms/' )),
    url( r'^forms/',            include( feedback.urls, namespace="ncc-forms" )),
    url( r'^admin/doc/',        include( 'django.contrib.admindocs.urls' )),
    url( r'^admin/',            include( admin.site.urls )),

    url( r'^paypal/',           include( 'paypal.standard.ipn.urls', namespace="ncc-paypal" )),
    url( r'^o/',                include( 'oauth2_provider.urls', namespace='oauth2_provider' )),
    url( r'^api-auth/',         include( 'rest_framework.urls', namespace='rest_framework') ),
    url( r'^api/',              include( 'api.urls', namespace='api' )),
]
