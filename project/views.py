from    django.contrib.auth.models          import User
from    django.core.mail                    import send_mail
from    django.http                         import HttpResponse, HttpResponseRedirect, Http404
from    django.shortcuts                    import render
from    django.template                     import loader, TemplateDoesNotExist
from    django.views.generic                import View


def message( request, msg, msg_class="danger" ):
    return render(
        request,
        'message.html',
        {
            "message":   msg,
            "msg_class": msg_class,
        },
    )

##################################################
#class ResetPassword(View):
##################################################

    #def post(self, request):
        #try:    user = User.objects.get( username=request.POST["username"] )
        #except  User.DoesNotExist:
            #return message( request, "Unable to reset password.  Contact administrator. (I)")

        #if not user.email:
            #return message( request, "Unable to reset password.  Contact administrator. (E)")

        #password = User.objects.make_random_password()
        #user.set_password(password)
        #user.save()

        #send_mail(
            #subject         = "Password Reset",
            #message         = "Your new password is '%s'." % password,
            #from_email      = "password_reset@forms.newcreation.org.sg",
            #recipient_list  = [user.email],
            #fail_silently   = False,
            #html_message    = "Your new password is '%s'." % password,
        #)

        #return message( request, "New password has been sent to your email address.", "success")

